------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
--
-- File                 : gestion_IO.vhd
-- Author               : 
-- Date                 : 07.11.2023
--
-- Context              : Avalon user interface
--
------------------------------------------------------------------------------------------
-- Description : 
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer    Comments
-- 0.0    See header              Initial version

------------------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity gestion_IO is
  port(
    clk_i               : in  std_logic;
    reset_i             : in  std_logic;

    button_i            : in std_logic_vector(3 downto 0);
    switch_i            : in std_logic_vector(9 downto 0);
    led_i               : in std_logic_vector(9 downto 0);
    cs_led_wr_i         : in std_logic;

    button_sync_o       : out std_logic_vector(3 downto 0);
    switch_sync_o       : out std_logic_vector(9 downto 0);
    led_o               : out std_logic_vector(9 downto 0)
  );
end gestion_IO;

architecture rtl of gestion_IO is
    constant ones_button_sized : std_logic_vector(button_i'range) := (others => '1');

begin
    process (clk_i, reset_i)
    begin
        if reset_i = '1' then
            led_o <= (others => '0');
            switch_sync_o <= switch_i;
            button_sync_o <= button_i xor ones_button_sized;
        elsif rising_edge(clk_i) then
            button_sync_o <= button_i xor ones_button_sized;
            switch_sync_o <= switch_i;
            if cs_led_wr_i = '1' then
                led_o <= led_i;
            end if;
        end if;
    end process;
    
end rtl; 