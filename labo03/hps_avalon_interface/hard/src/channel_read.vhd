------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
--
-- File                 : channel_read.vhd
-- Author               : 
-- Date                 : 07.11.2023
--
-- Context              : Avalon user interface
--
------------------------------------------------------------------------------------------
-- Description : 
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer    Comments
-- 0.0    See header              Initial version

------------------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    
entity channel_read is
  port(
    clk_i               : in  std_logic;
    reset_i             : in  std_logic;

    address_i           : in  std_logic_vector(13 downto 0);
    read_i              : in  std_logic;
    avalon_id_i         : in  std_logic_vector(31 downto 0);
    button_i            : in  std_logic_vector(3 downto 0);
    switch_i            : in  std_logic_vector(9 downto 0);
    led_i               : in  std_logic_vector(9 downto 0);
    connection_status_i : in  std_logic_vector(1 downto 0);
    busy_i              : in  std_logic;
    selection_i         : in  std_logic_vector(3 downto 0);
    data_i              : in  std_logic_vector(31 downto 0);

    read_data_valid_o   : out std_logic;
    read_data_o         : out std_logic_vector(31 downto 0)  
    
  );
end channel_read;

architecture rtl of channel_read is
    
    --| Signals declarations   |-------------------------------------------------------------- 
    signal read_data_s : std_logic_vector(31 downto 0);
    signal read_data_valid_s : std_logic;
begin

  process (clk_i, reset_i)
    --constant ZEROS : std_logic_vector(31 downto 0) := (others => '0');
    
    

  begin

    if reset_i = '1' then
        read_data_valid_o <= '0';
        read_data_s <= (others => '0');
    

    elsif rising_edge(clk_i) then

        if read_i = '1' then

            read_data_s <= (others => '0'); 
            
            case address_i(11 downto 0) is
                when x"000" =>
                    read_data_s <= avalon_id_i;
                when x"100" =>
                    read_data_s(3 downto 0) <= button_i;
                when x"110" =>
                    read_data_s(9 downto 0) <= switch_i;
                when x"120" =>
                    read_data_s(9 downto 0) <= led_i;
                when x"200" =>
                    read_data_s(1 downto 0) <= connection_status_i;
                when x"201" =>
                    read_data_s(0) <= busy_i;
                when x"210" =>
                    read_data_s(3 downto 0) <= selection_i;
                when x"220" =>
                    read_data_s <= data_i;
                when others =>
                    read_data_s <= x"CAFECAFE";
            end case;

            read_data_valid_o <= '1';


    
        else
            read_data_valid_o <= '0';
            read_data_s <= (others => 'Z');

        end if;

    end if;

  end process;

  read_data_o <= read_data_s;
    
end rtl; 
