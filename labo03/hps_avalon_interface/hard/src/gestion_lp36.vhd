------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
--
-- File                 : gestio_lp36.vhd
-- Author               : 
-- Date                 : 07.11.2023
--
-- Context              : Avalon user interface
--
------------------------------------------------------------------------------------------
-- Description : 
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer    Comments
-- 0.0    See header              Initial version

------------------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity gestion_lp36 is
    port(
        clk_i               : in  std_logic;
        reset_i             : in  std_logic;

        selection_wr_i      : in  std_logic;
        data_wr_i           : in  std_logic;
        selection_i         : in  std_logic_vector(3 downto 0);
        data_i              : in  std_logic_vector(31 downto 0);


        busy_o              : out std_logic;
        selection_o         : out std_logic_vector(3 downto 0);
        data_o              : out std_logic_vector(31 downto 0)
    );
end gestion_lp36;

architecture rtl of gestion_lp36 is

    --| Components declaration |--------------------------------------------------------------
    -- NONE
    
    --| Constants declarations |--------------------------------------------------------------
    constant TIMER_PERIOD : integer := 100; -- 1us (write cycle period) / 10ns (clock period)
    
    --| Signals declarations   |--------------------------------------------------------------  
    
    -- MSS signals
    type state_type is (WAITING, START_TRANSM, TRANSMIT);
    signal current_state_s, next_state_s: state_type;
    -- MSS outputs
    signal busy_s        : std_logic:= '0';
    signal start_trans_s : std_logic:= '0'; -- Start transmission

    -- Timer signals
    signal timer_count_s : integer := 0;
    signal timer_reset_s : std_logic := '0';
    signal timer_end_s   : std_logic := '0';
    signal timer_en_s    : std_logic := '0';

    -- Register signals
    signal selection_reg_s : std_logic_vector(3 downto 0) := (others => '0');
    signal data_reg_s      : std_logic_vector(31 downto 0) := (others => '0');

begin

    -- MSS Process
    MSS_Process_Ctl: process(clk_i, reset_i)
    begin
        if reset_i = '1' then
            current_state_s <= WAITING;  -- Initialize to INIT state on reset
        elsif rising_edge(clk_i) then
            current_state_s <= next_state_s;
        end if;
    end process;

    MSS_Process: process(current_state_s, data_wr_i, timer_end_s)
    begin
        -- Default next state
        next_state_s <= current_state_s;

        -- Initialize the signals
        busy_s <= '0';
        start_trans_s <= '0';

        case current_state_s is
            when WAITING =>
                if data_wr_i = '1' then
                    next_state_s <= START_TRANSM;
                end if;

            when START_TRANSM =>
                busy_s <= '1';
                start_trans_s <= '1';
                next_state_s <= TRANSMIT;
                
            when TRANSMIT =>
                busy_s <= '1';
                start_trans_s <= '0';
                if timer_end_s = '1' then
                    next_state_s <= WAITING;
                end if;
        end case;
    end process;

    timer_reset_s <= start_trans_s;
    timer_en_s <= busy_s;
    -- Timer process
    Timer_Process: process(clk_i, reset_i)
    begin
        if reset_i = '1' then
            timer_count_s <= 0;  -- Initialize timer count on reset
            timer_end_s <= '0';  -- Initialize timer_end_s on reset
        elsif rising_edge(clk_i) then
            if timer_reset_s = '1' then
                timer_count_s <= 0;
                timer_end_s <= '0';
            else
                if timer_en_s = '1' then
                    if timer_count_s = TIMER_PERIOD then  -- Set the timer period
                        timer_end_s <= '1';  -- Timer has expired
                        timer_count_s <= 0;
                    else
                        timer_count_s <= timer_count_s + 1;
                    end if;
                end if;
            end if;
        end if;
    end process;

    -- Register process for selection and data
    Register_Process: process(clk_i, reset_i)
    begin
        if reset_i = '1' then
            -- Initialize registers on reset
            selection_reg_s <= (others => '0');
            data_reg_s <= (others => '0');
        elsif rising_edge(clk_i) then
            if selection_wr_i = '1' and busy_s = '0' then
                selection_reg_s <= selection_i; -- Update selection register
            end if;
            if data_wr_i = '1' and busy_s = '0' then
                data_reg_s <= data_i; -- Update data register
            end if;
        end if;
    end process;

    -- OUTPUT set
    selection_o <= selection_reg_s;
    data_o <= data_reg_s;
    busy_o <= busy_s;
end rtl; 