------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
--
-- File                 : channel_write.vhd
-- Author               : 
-- Date                 : 07.11.2023
--
-- Context              : Avalon user interface
--
------------------------------------------------------------------------------------------
-- Description : 
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer    Comments
-- 0.0    See header              Initial version

------------------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity channel_write is
  port(
    clk_i               : in  std_logic;
    reset_i             : in  std_logic;

    address_i           : in  std_logic_vector(13 downto 0);
    write_i             : in  std_logic;
    busy_i              : in  std_logic;
    write_data_i        : in  std_logic_vector(31 downto 0);

    cs_led_wr_o         : out std_logic;
    led_o               : out std_logic_vector(9 downto 0);
    selection_o         : out std_logic_vector(3 downto 0);
    data_o              : out std_logic_vector(31 downto 0);
    cs_data_wr_o        : out std_logic;
    cs_selection_wr_o   : out std_logic  
    
  );
end channel_write;

architecture rtl of channel_write is

    --| Components declaration |--------------------------------------------------------------
    
    --| Constants declarations |--------------------------------------------------------------
    
    --| Signals declarations   |--------------------------------------------------------------   
signal write_data_s : std_logic_vector(31 downto 0);

begin

  process (clk_i, reset_i)
  begin

    if reset_i = '1' then
        data_o <= (others => '0');
        led_o <= (others => '0');
        selection_o <= (others => '0');


    elsif rising_edge(clk_i) then

      cs_led_wr_o <= '0';
      cs_selection_wr_o <= '0';
      cs_data_wr_o  <= '0';

      if busy_i = '0' and write_i = '1' then 
        case address_i(11 downto 0) is
          when x"120" =>
              cs_led_wr_o <= '1';
              led_o <= write_data_i(9 downto 0);

          when x"210" =>
              cs_selection_wr_o <= '1';
              selection_o <= write_data_i(3 downto 0);

          when x"220" =>
              cs_data_wr_o <= '1';
              data_o <= write_data_i;

          when others =>
              data_o <= x"CAFECAFE";

        end case;        
      end if;
    end if;

  end process;

end rtl; 