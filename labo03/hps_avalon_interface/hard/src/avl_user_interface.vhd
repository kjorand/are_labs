------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : avl_user_interface.vhd
-- Author               : 
-- Date                 : 04.08.2022
--
-- Context              : Avalon user interface
--
------------------------------------------------------------------------------------------
-- Description : 
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer    Comments
-- 0.0    See header              Initial version

------------------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    
entity avl_user_interface is
  port(
    -- Avalon bus
    avl_clk_i           : in  std_logic; 
    avl_reset_i         : in  std_logic; 
    avl_address_i       : in  std_logic_vector(13 downto 0);
    avl_byteenable_i    : in  std_logic_vector(3 downto 0); -- unused in this lab
    avl_write_i         : in  std_logic; 
    avl_writedata_i     : in  std_logic_vector(31 downto 0);
    avl_read_i          : in  std_logic; 
    avl_readdatavalid_o : out std_logic; 
    avl_readdata_o      : out std_logic_vector(31 downto 0); 
    avl_waitrequest_o   : out std_logic;
    -- User interface
    boutton_i           : in  std_logic_vector(3 downto 0); 
    switch_i            : in  std_logic_vector(9 downto 0); 
    led_o               : out std_logic_vector(9 downto 0); 
    lp36_we_o           : out std_logic; 
    lp36_sel_o          : out std_logic_vector(3 downto 0); 
    lp36_data_o         : out std_logic_vector(31 downto 0);
    lp36_status_i       : in  std_logic_vector(1 downto 0) 
  );
end avl_user_interface;

architecture rtl of avl_user_interface is

    --| Components declaration |--------------------------------------------------------------

    component channel_write
      port(
        clk_i               : in  std_logic;
        reset_i             : in  std_logic;

        address_i           : in  std_logic_vector(13 downto 0);
        write_i             : in  std_logic;
        busy_i              : in  std_logic;
        write_data_i        : in  std_logic_vector(31 downto 0);

        cs_led_wr_o         : out std_logic;
        led_o               : out std_logic_vector(9 downto 0);
        selection_o         : out std_logic_vector(3 downto 0);
        data_o              : out std_logic_vector(31 downto 0);
        cs_data_wr_o        : out std_logic;
        cs_selection_wr_o   : out std_logic  
    
      );
    
    end component;

    component channel_read
      port(
        clk_i               : in  std_logic;
        reset_i             : in  std_logic;

        address_i           : in  std_logic_vector(13 downto 0);
        read_i              : in  std_logic;
        avalon_id_i         : in  std_logic_vector(31 downto 0);
        button_i            : in  std_logic_vector(3 downto 0);
        switch_i            : in  std_logic_vector(9 downto 0);
        led_i               : in  std_logic_vector(9 downto 0);
        connection_status_i : in  std_logic_vector(1 downto 0);
        busy_i              : in  std_logic;
        selection_i         : in  std_logic_vector(3 downto 0);
        data_i              : in  std_logic_vector(31 downto 0);

        read_data_valid_o   : out std_logic;
        read_data_o         : out std_logic_vector(31 downto 0)  
      
      );
    end component;    

    component gestion_IO
      port(
        clk_i               : in  std_logic;
        reset_i             : in  std_logic;

        button_i            : in std_logic_vector(3 downto 0);
        switch_i            : in std_logic_vector(9 downto 0);
        led_i               : in std_logic_vector(9 downto 0);
        cs_led_wr_i         : in std_logic;

        button_sync_o       : out std_logic_vector(3 downto 0);
        switch_sync_o       : out std_logic_vector(9 downto 0);
        led_o               : out std_logic_vector(9 downto 0)
      );
    end component;

    component gestion_lp36
      port(
        clk_i               : in  std_logic;
        reset_i             : in  std_logic;

        selection_wr_i      : in  std_logic;
        data_wr_i           : in  std_logic;
        selection_i         : in  std_logic_vector(3 downto 0);
        data_i              : in  std_logic_vector(31 downto 0);

        busy_o              : out std_logic;
        selection_o         : out std_logic_vector(3 downto 0);
        data_o              : out std_logic_vector(31 downto 0)
      );
    end component;

    --| Constants declarations |--------------------------------------------------------------
    
    --| Signals declarations   |--------------------------------------------------------------   

    signal button_s : std_logic_vector(3 downto 0);
    signal switch_s : std_logic_vector(9 downto 0);
    signal led_wr_s : std_logic_vector(9 downto 0);
    signal led_rd_s : std_logic_vector(9 downto 0);
    signal cs_led_wr_s : std_logic; 

    signal busy_s : std_logic;
    signal selection_s : std_logic_vector(3 downto 0);

    signal avalon_id_s : std_logic_vector(31 downto 0);

    signal cs_data_wr_s : std_logic;      
    signal cs_selection_wr_s : std_logic;

    signal data_max_s : std_logic_vector(31 downto 0);
    
begin

    avl_waitrequest_o <= '0';

    avalon_id_s <= x"BEEFBEEF";

    -- Read access part

    channel_read_avl_user_interface : channel_read
      port map (
        clk_i       => avl_clk_i,             
        reset_i     => avl_reset_i,       

        address_i   => avl_address_i,      
        read_i      => avl_read_i,        
        avalon_id_i => avalon_id_s,      
        button_i    => button_s,        
        switch_i    => switch_s,       
        led_i       => led_rd_s, 

        connection_status_i => lp36_status_i,

        busy_i      => busy_s,      
        selection_i => selection_s,      
        data_i      => data_max_s,     

        read_data_valid_o => avl_readdatavalid_o,
        read_data_o =>  avl_readdata_o       
      );
    
    -- Write access part

    channel_write_avl_user_interface : channel_write
      port map (
        clk_i         => avl_clk_i,           
        reset_i       => avl_reset_i,           

        address_i     => avl_address_i,           
        write_i       => avl_write_i,           
        busy_i        => busy_s,           
        write_data_i  => avl_writedata_i,        

        cs_led_wr_o  => cs_led_wr_s,         
        led_o        => led_wr_s,         
        selection_o  => selection_s,         
        data_o       => data_max_s,      
        cs_data_wr_o  => cs_data_wr_s,       
        cs_selection_wr_o => cs_selection_wr_s  
      );
    
    -- Interface management

    gestion_l36_avl_user_interface : gestion_lp36
      port map(
        clk_i    => avl_clk_i,             
        reset_i  => avl_reset_i,

        selection_wr_i  => cs_selection_wr_s,  
        data_wr_i   => cs_data_wr_s,       
        selection_i => selection_s,       
        data_i      => data_max_s,      

        busy_o  => busy_s,            
        selection_o => lp36_sel_o,      
        data_o   => lp36_data_o         
      );

    lp36_we_o <= busy_s;
    led_o <= led_rd_s;

    -- sync signal
    gestion_IO_avl_user_interface : gestion_IO
  
      port map (
        clk_i         => avl_clk_i,             
        reset_i       => avl_reset_i,      

        button_i      => boutton_i,      
        switch_i      => switch_i,
        led_i         => led_wr_s,
        cs_led_wr_i   => cs_led_wr_s,     

        button_sync_o => button_s,   
        switch_sync_o => switch_s,      
        led_o         => led_rd_s      
      );
  
end rtl; 