LABO 03 : Avalon interface
==========================

ARE 23-24

Rachel Leupi, Kévin Jorand

--------------------------

## Objectif

L'objectif de ce laboratoire est de créer une interface simple sur le bus Avalon connecté au processeur HPS. Pour ce faire, il est nécessaire de définir un plan d'adressage permettant d'accéder et de contrôler les IO de la MAX10 et de la DE1-SoC. Les dispositifs d'entrée/sortie de la carte DE1-SoC, tels que les LEDs, les boutons et les interrupteurs, seront utilisés, ainsi qu'une liaison parallèle avec 36 liens directs provenant de la carte MAX10_leds. Ensuite, il sera nécessaire de coder notre design en VHDL et de rédiger un programme conforme aux spécifications pour piloter cette interface.

## Réponse au question 

**Question 1**
> _Étudier le fonctionnement du bus Avalon à l’aide du document fourni `Description_Bus_Avalon.pdf`. Dans le plan d’adressage, la taille de la zone disponible pour votre interface correspond telle aux 14 bits d’adresse défini dans le bus Avalon ? Pourquoi ?_

Non, la zone disponible est de 16 bits (point de vu CPU). Au niveau CPU, la mémoire est adressée par byte, alors que sur le bus Avalon, elle est adressée par registre de 32 bits. Donc les deux bits de LSB sont toujours à 0 et donc inutiles. Ils sont donc retirés du bus Avalon.

## Plan d'adressage

Considérant que nous avons une zone **très** importante à disposition, nous avons pris le parti de séparer notre zone d'adressage de la manière ci-dessous. Il est à noter qu'à chaque niveau d'indentation supplémentaire, l'offset se rapporte au niveau supérieur (ils sont donnés en termes d'adresses Avalon, donc adressé par registres de 32 bits), la base étant l'adresse de début assignée à notre design sur le bus `AXI lightweight HPS-to-FPGA` :
- Avalon ID                   [offset 0x0000]
- Périphériques DE1-SoC       [offset 0x0100]
    - Boutons                 [offset 0x0000]
    - Switchs                 [offset 0x0010]
    - Leds                    [offset 0x0020]
- MAX10 (LP36)                [offset 0x0200]
    - Board Connection Status [offset 0x0000]
    - Transmission Status     [offset 0x0001]
    - Selection               [offset 0x0010]
    - Data                    [offset 0x0020]

Cela nous permet d'avoir quelque chose d'extrêmement lisible, même directement en lisant les adresses en hexadécimal. Cela a l'énorme avantage de laisser de la place dans notre plan d'adressage pour tout ajout futur, comme par exemple pour les leds des registres `outset`, `outclear`, `rising_edge`, ... etc tout en gardant le tout dans le même bloc d'adresse. De même on pourrait ajouter d'autres périphériques de la DE1-SoC sans aucun problème. Notre zone d'adresse (point de vu Avalon) s'étend ainsi sur la plage `0x000-0x2FF`, soit 10 bits sur les 14 fournis, laissant toujours suffisamment de "place" pour tout ajout ultérieur sur le bus. 

Voici le plan d'adressage effectué : 

| Offset CPU | Adresse Avalon | Read                                     | Write                              | Commentaire                     |
| ---------- | -------------- | ---------------------------------------- | ---------------------------------- | ------------------------------- |
| 0x0000     | 0x0000         | AvalonID[31..0]                          | Reserved                           | ID du bus Avalon                |
| 0x0400     | 0x0100         | Boutons state[3..0]<br>Unused[31..4]     | Reserved                           | DE1-SoC - Boutons               |
| 0x0440     | 0x0110         | Switchs state[9..0]<br>Unused[31..10]    | Reserved                           | DE1-SoC - Switchs               |
| 0x0480     | 0x0120         | Leds[9..0]<br>Unused[31..10]             | Leds[9..0]<br>Reserved[31..10]     | DE1-SoC - Leds                  |
| 0x0800     | 0x0200         | Connection Status[1..0]<br>Unused[31..2] | Reserved                           | MAX10 - Board Connection Status |
| 0x0804     | 0x0201         | Busy[0]<br>Unused[31..1]                 | Reserved                           | MAX10 - Transmission Status     |
| 0x0840     | 0x0210         | Selection[3..0]<br>Unused[31..4]         | Selection[3..0]<br>Reserved[31..4] | MAX10 - Selection               |
| 0x0880     | 0x0220         | Data[31..0]                              | Data[31..0]                        | MAX10 - Data & Envoi à MAX 10   |

Après consultation auprès du "client" (l'assistant), nous avons pris la décision suivante : il est de la responsabilité de l'utilisateur de vérifier le flag `Busy` avant de tenter une écriture sur `Selection` ou `Data`. Une écriture alors que le flag est activé n'aurait simplement aucun effet.

_Note: Après coup, nous nous sommes rendus compte qu'il aurait certainement été malin d'implémenter le "busy" sous la forme de la valeur courante du timer (qui ne tournerait que pendant un envoi) ... ainsi l'utilisateur qui le lit peut avoir une estimation du temps d'attente restant et s'éviter de "poller" trop souvent._

## Conception du design

Nous avons choisi de diviser notre design en plusieurs sous module qui sont les suivants :
- Gestion IO
- Channel Read
- Channel Write
- Gestion Lp36

**Gestion IO** : Ce module est responsable de synchroniser les entrées et de permettre la relecture des leds. Pour cela chaque signal passe au travers d'une flip flop. Les leds quand à elles sont sauvegardées au travers d'un registre 10 bits.

**Channel Read** : Ce composant agit comme un pont entre le bus Avalon et les différentes entrées du système, permettant la lecture sélective des données en fonction de l'adresse spécifiée.

un multiplexeur piloté par l'adresse de lecture envoyé par le bus avalon permet de lire l'entrée( qui peut provenir de la MAX10 ou de la DE1-SoC) souhaitée.

lors de la lecture un signal `read_data_valid_o` permet de prendre connaissance du moment où la valeur est disponible.

**Channel Write** : Comme le module channel read, channel write sert de pont entre les sortie du système et le bus Avalon. Il récupère la valeurs et écrit la données reçu sur une des sorties de la MAX10 ou de la DE1-SoC. Comme précédemment un multiplexeur piloté par l'adresse d'écriture envoyée par le bus avalon permet
de mettre à jour la sortie souhaité.

les deux modules Channel read et Channel write sont activé grâce à des signaux `read_i` et `write_i` issus du bus Avalon.

**Gestion lp36** : Ce bloc est essentiellement composé de 3 sous-blocs (*mais implémenté dans un seul fichier VHDL*), à savoir :
- une machine d'état qui permet de maintenir l'état de la communication sur le bus lp36, qui pilote directement les sorties à l'exception des données et qui contrôle les accès aux registres
- un compteur qui permet de mesurer facilement la 1μs de maintient lors des transferts
- deux registres permettant de mémoriser et maintenir les données à mettre sur le bus

On trouvera ci-dessous le graphe décrivant le fonctionnement de la machine d'état et l'état des sorties associées. Le compteur et les registres étant relativement simples, nous ne les détaillerons pas plus avant. 

![MSS - Graphe d'état](./imgs/fsm.svg)


Cette structuration en modules distincts offre une clarté dans la gestion des entrées/sorties et facilite la synchronisation des divers composants de notre conception.

Voici un schéma bloc de notre design : 

![image](./imgs/Schema_bloc.jpg)

## Simulation sur Quartus

Voici une vue RTL de notre design VHDL.
![image](./imgs/Schema_interface_quartus1.png)


Voici une vue RTL de notre interface englobée dans le design complet.
![image](./imgs/schema_interface_quartus.png)

Il n'y a pas grand chose à rajouter ici notre design VHDL final est une copie du schéma bloc imaginé en premier lieu.
Il reste néanmoins intéressant de voir le design complet du système et de constater la façon de notre interface d’interagir avec le reste des composants/modules. 

## Programmation C

Rien de particulier à signaler à ce niveau. On notera tout de même les deux fonctions suivantes :
```c
void set_data_to_MAX10(uint32_t data, uint8_t sel);
void read_key(uint8_t *state, uint8_t *rising_edge);
```
Elles ont en effet un comportement quelque peu plus complexe qu'une simple lecture sur un bus avec un masque. 

La première attend que le bus soit disponible puis procède à l'écriture de la `selection` et ensuite des `data`. Tout est intégré en une seule fonction.

La seconde permet d'aller relever l'état des `keys` (ou boutons). Cependant, afin d'éviter une seconde lecture des ces derniers (et potentiellement un état incohérent avec la première) et d'éviter à l'utilisateur de devoir implémenter une détection de flanc, on fait aussi cette dernière dans la fonction, d'où les deux paramètres de sorties.

## Tests effectués

### Simluation (QuestaSim)

Nous avons commencé par simuler notre design avec QuestaSim grâce au testbench et à la console fournis. Cela nous a permis d'attraper les quelques coquilles VHDL (mais pratiquement aucune) ... Et nous avons aussi constaté une petite "erreur" dans la console: les entrées n'étant pas reportées avant une opération sur le bus (malgré les "périodes" que l'on peut "run").

### Sur la carte

Afin de s'assurer du bon fonctionnement de notre design VHDL, nous avons utilisé la section memory de Arm Développement studio afin d'y effectué des lectures ainsi que des écriture au adresse de nos IO. 

voici un exemple ici avec la lecture de notre constante à l'adresse 0xFF210000:

![image](./imgs/beefbeef.jpeg)


## Conclusion
En conclusion, ce laboratoire nous a permis de concevoir une interface sur le bus Avalon, connecté au processeur HPS, pour interagir avec les périphériques de la carte DE1-SoC et la liaison parallèle avec 36 liens directs de la carte MAX10_leds. Nous avons élaboré un plan d'adressage permettant un accès clair et organisé aux différents composants, tout en laissant de la flexibilité pour d'éventuels ajouts futurs.

La conception du design en VHDL a été structurée de manière modulaire, avec des modules dédiés à la gestion des entrées/sorties, à la lecture et à l'écriture sur le bus Avalon, ainsi qu'à la gestion du bus lp36. Cette approche modulaire facilite la compréhension, le débogage et la maintenance du code ainsi que pour une éventuelle nouvelle utilisation dans les prochain laboratoire.

La simulation sur Quartus a été réalisée pour valider le bon fonctionnement du design VHDL. La vue RTL a permis de vérifier l'interconnexion correcte des différents modules.





