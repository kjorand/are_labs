/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : hps_application.c
 * Author               : 
 * Date                 : 
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: Conception d'une interface simple sur le bus Avalon avec la carte DE1-SoC
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 
 *
*****************************************************************************************/
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include "axi_lw_avalon.h"
#include "io_functions.h"

// Our data sources
#define SRC_SWITCH 0x0
#define SRC_PAT_1  0x1
#define SRC_PAT_2  0x2
#define SRC_ONES   0x3

int __auto_semihosting;

void allMax10LedsOff(){
    set_data_to_MAX10(0x00000000, SEL_LED_SECOND_HIGH);
    set_data_to_MAX10(0x00000000, SEL_LED_SECOND_LOW);
    set_data_to_MAX10(0x00000000, SEL_LED_2LINES);
    set_data_to_MAX10(0x00000000, SEL_LED_SQUARE);
}

int main(void){
    
    printf("Laboratoire: Conception d'une interface simple \n");

    // Check LP36 connection status
    if(read_lp36_status() != LP36_VALID_STATUS){
        printf("Connection status error : 0x%"PRIX8" \n", read_lp36_status());
        return 1;
    }

    // Turn all DE1-SoC leds and MAX10 leds off
    set_leds(0x0000);
    allMax10LedsOff();

    // Read and print constants / ID
    printf("AXI ID : 0x%"PRIX32" \n", read_axi_lw_ID());
    printf("AVALON ID : 0x%"PRIX32" \n", read_avalon_ID());
    
    // END INIT !

    uint8_t keys;
    uint8_t keys_10;
    uint8_t keys_pressed;
    uint8_t switchsShift = 0;
    uint8_t sel;
    uint16_t switchs;
    uint32_t data;
    while(1){
        // Read keys
        read_key(&keys, &keys_pressed);

        // Read switchs
        switchs = read_switch();

        // Report switchs on leds
        set_leds(switchs);

        // Set selection bits
        sel = (uint8_t)(switchs >> 8);

        // Choose data source
        keys_10 = keys & (KEY0 | KEY1);
        switch (keys_10) {
            case SRC_SWITCH:
                /* 00 : Copie de la valeur des 8 interrupteurs (SW0 to SW7). */
                data = (uint32_t)switchs & 0xFF; // Truncate
                break;
            
            case SRC_PAT_1:
                /* 01 : Afficher la valeur 1100…1100. */
                data = 0xCCCCCCCC;
                break;
            
            case SRC_PAT_2:
                /* 10 : Afficher la valeur 0011…0011. */
                data = 0x33333333;
                break;
            
            case SRC_ONES:
                /* 11 : Afficher la valeur 1111…1111. */
                data = 0xFFFFFFFF;
                break;
            
            default:
                printf("FATAL ERROR in buttons (keys) value (0x%"PRIX8")\n", keys);
                break;
        }

        // Increment the shifting of the switch data in case of key press
        if(keys_pressed & KEY2){
            switchsShift += 8;
            switchsShift %= 32;
        }

        // Turn off MAX10 leds
        if(keys_pressed & KEY3){ // /* For all off on hold => */ if(keys & KEY3){
            allMax10LedsOff();
        }else{
            if(sel == SEL_LED_2LINES && keys_10 == SRC_SWITCH){
                data <<= switchsShift;
            }
            set_data_to_MAX10(data, sel);
        }
    }
}
