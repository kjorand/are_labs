/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : io_functions.h
 * Author               : Leupi & Jorand
 * Date                 : 06.11.2023
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: Header file for io functions
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 0.0    06.11.2023  RLI & KJD    Initial version.
 *
*****************************************************************************************/

#ifndef __IO_FUNCTIONS_H__
#define __IO_FUNCTIONS_H__
#include <stdint.h>

//***********************************//
//****** Global usage function ******//

/// @brief After ensuring the lp36 bus is available, it sets the selection and then the data to the corresponding lp36 registers
/// @param data The data to set on the MAX10 leds
/// @param sel  The selection of the destination (from the different "zones" of the MAX10)
void set_data_to_MAX10(uint32_t data, uint8_t sel);

/// @brief Reads the keys register and fills the state and edge passed in parameter as long as not null.
/// Information is represented as bitfield : each bit representing one key
/// @param state receives the current state of the buttons
/// @param rising_edge  receives the rising edge of the buttons : every button which saw a rising edge since last call is set to '1' others to '0'
void read_key(uint8_t *state, uint8_t *rising_edge);

/// @brief Reads the value on the switches
/// @return The state of the switches, one bit for each
uint16_t read_switch();

/// @brief Puts the argument on the leds (each led representing one bit for the LSBs)
/// @param leds The value to put on leds
void set_leds(uint16_t leds);

/// @brief Reads the avalon ID constant
/// @return The avalon ID constant
uint32_t read_avalon_ID();

/// @brief Reads the AXI lightweight bus constant ID
/// @return The AXI lightweight bus constant ID
uint32_t read_axi_lw_ID();

/// @brief Reads the MAX10 connection status on the lp36
/// @return The status
uint8_t read_lp36_status();

#endif /* __IO_FUNCTIONS_H__ */
