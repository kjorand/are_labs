/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : io_functions.c
 * Author               : Leupi & Jorand
 * Date                 : 06.11.2023
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: io functions
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 0.0    06.11.2023  RLI & KJD    Initial version.
 *
*****************************************************************************************/

#include <stdint.h>
#include "axi_lw_avalon.h"
#include "io_functions.h"

void set_data_to_MAX10(uint32_t data, uint8_t sel){
    LP36NOT_BUSY;    

    AVALON_REG(REG_SELECTION) = sel & SELECTION_BITS;
    AVALON_REG(REG_DATA) = data;
}

void read_key(uint8_t *state, uint8_t *rising_edge) {
    static uint8_t lastValue = 0x0;
    uint8_t currentValue = (AVALON_REG(REG_BOUTONS) & KEY_BITS);
    if(state){
        *state = currentValue;
    }
    if(rising_edge){
        *rising_edge = (~lastValue) & currentValue; // forces last = 0 and current = 1
    }

    lastValue = currentValue;
}

uint16_t read_switch(){
    return (uint16_t)(AVALON_REG(REG_SWITCHS) & SWITCHS_BITS);
}

void set_leds(uint16_t leds){
    AVALON_REG(REG_LEDS) = (AVALON_REG(REG_LEDS) & ~LEDS_BITS) | (leds & LEDS_BITS);
}

uint32_t read_avalon_ID(){
    return AVALON_REG(REG_AVALONID);
}

uint32_t read_axi_lw_ID(){
    return AXI_LW_REG(REG_AXI_LW_ID);
}

uint8_t read_lp36_status(){
    return (uint8_t)(AVALON_REG(REG_CONNECTION) & CONECTION_BITS);
}

