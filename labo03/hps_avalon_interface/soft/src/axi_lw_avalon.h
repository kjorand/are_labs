/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : axi_lw.h
 * Author               : Anthony Convers
 * Date                 : 27.07.2022
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: Header file for bus AXI lightweight HPS to FPGA defines definition
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 0.0    27.07.2022  ACS           Initial version.
 *
*****************************************************************************************/
#include <stdint.h>

// Base address
#define AXI_LW_HPS_FPGA_BASE_ADD    0xFF200000 /* source : p. 2-16 */
#define AVALON_BASE_ADD             0x00010000

// AXI registers
#define REG_AXI_LW_ID  0x0000

// Avalon registers
#define REG_AVALONID   0x0000 /* Avalon : 0x0000 - AvalonID[31..0]                         / Reserved                          - ID du bus Avalon                */
#define REG_BOUTONS    0x0400 /* Avalon : 0x0100 - Boutons state[3..0] - Unused[31..4]     / Reserved                          - DE1-SoC - Boutons               */
#define REG_SWITCHS    0x0440 /* Avalon : 0x0110 - Switchs state[9..0] - Unused[31..10]    / Reserved                          - DE1-SoC - Switchs               */
#define REG_LEDS       0x0480 /* Avalon : 0x0120 - Leds[9..0] - Unused[31..10]             / Leds[9..0] - Reserved[31..10]     - DE1-SoC - Leds                  */
#define REG_CONNECTION 0x0800 /* Avalon : 0x0200 - Connection Status[1..0] - Unused[31..2] / Reserved                          - MAX10 - Board Connection Status */
#define REG_BUSY       0x0804 /* Avalon : 0x0201 - Busy[0] - Unused[31..1]                 / Reserved                          - MAX10 - Transmission Status     */
#define REG_SELECTION  0x0840 /* Avalon : 0x0210 - Selection[3..0] - Unused[31..4]         / Selection[3..0] - Reserved[31..4] - MAX10 - Selection               */
#define REG_DATA       0x0880 /* Avalon : 0x0220 - Data[31..0]                             / Data[31..0]                       - MAX10 - Data & Envoi à MAX 10   */

// ACCESS MACROS
#define AVALON_REG(_x_)   *(volatile uint32_t *)(AXI_LW_HPS_FPGA_BASE_ADD + AVALON_BASE_ADD + _x_) // _x_ is a "CPU" offset with respect to the avalon base address
#define AXI_LW_REG(_x_)   *(volatile uint32_t *)(AXI_LW_HPS_FPGA_BASE_ADD + _x_) /* _x_ is a "CPU" offset with respect to the base address */

// Define bits usage
#define SWITCHS_BITS   0x000003FF
#define LEDS_BITS      0x000003FF
#define KEY_BITS       0x0000000F
#define CONECTION_BITS 0x00000003
#define BUSY_BITS      0x00000001
#define SELECTION_BITS 0x0000000F

// Individual key's bits
#define KEY0 0x1
#define KEY1 0x2
#define KEY2 0x4
#define KEY3 0x8

// Valid status constant
#define LP36_VALID_STATUS 0x1 /* 01 : configuration valide. */ 

// Selection values
#define SEL_LED_SECOND_LOW  0x0 /*0000 lp36_data31-30 non utilisés, Leds secondes DS30.. .1*/
#define SEL_LED_SECOND_HIGH 0x1 /*0001 lp36_data31-30 non utilisés, Leds secondes DS60...31*/
#define SEL_LED_2LINES      0x2 /*0010 Les 2 lignes de leds DL31...16 & DL15....0*/
#define SEL_LED_SQUARE      0x3 /*0011 lp36_data 31-25 non utilisés, Carré de leds DM11-15/21-25/31-35/41-45/51-55 */

/* Polls until LP36 isn't busy anymore */
#define LP36NOT_BUSY     while(AVALON_REG(REG_BUSY))
