/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : io_functions.c
 * Author               : Leupi & Jorand
 * Date                 : 09.01.2024
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: UART functions
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 0.0    09.01.2024  RLI & KJD    Initial version.
 *
*****************************************************************************************/

#include "uart.h"

#include <string.h>
#include <stdio.h>

//***********************************//
//***** Private usage functions *****//

/// @brief Sets the UART0 baud rate
void setUart0BaudRate();

/// @brief Enables the fifos for UART0
void enableUart0Fifo();

/// @brief Disables UART0 parity
void disableUart0Partity();

/// @brief Sets UART0 stop bit to 1
void setUart0StopBit();

/// @brief Ensures uart0 driven by HPS
void setSysMgr();

/// @brief Sets UART0 char bit length
void setUart0CharBitLen();

/// @brief Sends the string over via UART0
/// @param str the string to send of size max of 128 bytes
/// @return the number of chars sent, 0 in case of error or -ERROR code
size_t sendUart0Chunk(const char* str);

//***********************************//
//****** Function definitions *******//

void initUart0() {
    setSysMgr();
    setUart0BaudRate();
    setUart0CharBitLen();
    setUart0StopBit();
    disableUart0Partity();
    enableUart0Fifo();
}

void setSysMgr(){
    SYSMGR_REG(REG_UART0USEFPGA) &= ~BITS_FPGA;
	SYSMGR_REG(REG_GENERALIO14) |= BITS_SEL_UART;
}

int sendUart0(const char *str) {
    size_t len = strlen(str);
    size_t sent = 0;
    // if(len > MAX_SEND){
    //     printf("[%s] ERROR : Too damn long", __FUNCTION__);
    //     return 1;
    // }
    while(sent < len){
        sent += sendUart0Chunk(str + sent);
    }
    return 1;
}

void setUart0BaudRate() {
    // Enables reading and writing of the Divisor Latch register (DLL and DLH) to set the baud rate of the UART. This bit must be cleared after initial baud rate setup in order to access other registers.
    UART_REG(REG_LCR) |= BITS_DLAB;

    // Sets value
    UART_REG(REG_RBR_THR_DLL) = (UART_REG(REG_RBR_THR_DLL) & ~BITS_DLL) | (BAUDRATE_CLK_DIV & 0xFF);
    UART_REG(REG_IER_DLH) = (UART_REG(REG_IER_DLH) & ~BITS_DLH) | ( (BAUDRATE_CLK_DIV >> 8) & 0xFF);

    // Cleares DLAB bit after initial baud rate setup in order to access other registers.
    UART_REG(REG_LCR) &= ~BITS_DLAB;
}

void enableUart0Fifo() {
    UART_REG(REG_FCR) |= BITS_FIFOE;
    UART_REG(REG_IER_DLH) |= BITS_TX_HOLD_EMPTY;
}

void disableUart0Partity() {
    UART_REG(REG_LCR) &= ~BITS_PEN;
}

void setUart0StopBit() {
    UART_REG(REG_LCR) &= ~BITS_STOP;
}

void setUart0CharBitLen() {
    UART_REG(REG_LCR) |= BITS_DLS_8;
}

size_t sendUart0Chunk(const char *str) {
    size_t i = 0;
    while(str[i] != '\0'){
        while(!(UART_REG(REG_LSR) & BITS_TEMT)); // Wait until empty before transmitting new one
        for(int j = 0; str[i] != '\0' && j < MAX_SEND; ++i, ++j){
            UART_REG(REG_RBR_THR_DLL) = str[i];
        }
    }
    return i;
}
