/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : uart.h
 * Author               : Leupi Rachel & Jorand Kévin
 * Date                 : 09.01.2024
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: Header file for UART0 control
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 0.0    09.01.2024  ACS           Initial version.
 *
*****************************************************************************************/

#ifndef __UART_H__
#define __UART_H__

#include <stdint.h>

// Base address
#define UART_BASE_ADD  0xFFC02000 /* p. 2-19 */

// Reset not necessary since performed by hardware on cold and warm reset

// The output baud rate is equal to the serial clock l4_sp_clk frequency divided by sixteen
// times the value of the baud rate divisor

// UART registers
#define REG_RBR_THR_DLL 0x0  /* on page 22-15 - RW - Default 0x0        - Rx Buffer, Tx Holding, and Divisor Latch Low */
#define REG_IER_DLH     0x4  /* on page 22-17 - RW - Default 0x0        - Interrupt Enable and Divisor Latch High */
#define REG_IIR         0x8  /* on page 22-19 - RO - Default 0x1        - Interrupt Identity Register (when read) */
#define REG_FCR         0x8  /* on page 22-20 - WO - Default 0x0        - FIFO Control (when written) */
#define REG_LCR         0xC  /* on page 22-23 - RW - Default 0x0        - Line Control Register (When Written) */
#define REG_MCR         0x10 /* on page 22-25 - RW - Default 0x0        - Modem Control Register */
#define REG_LSR         0x14 /* on page 22-28 - RO - Default 0x60       - Line Status Register */
#define REG_MSR         0x18 /* on page 22-31 - RO - Default 0x0        - Modem Status Register */
#define REG_SCR         0x1C /* on page 22-35 - RW - Default 0x0        - Scratchpad Register */
#define REG_SRBR        0x30 /* on page 22-35 - RW - Default 0x0        - Shadow Receive Buffer Register */
#define REG_STHR        0x34 /* on page 22-36 - RW - Default 0x0        - Shadow Transmit Buffer Register */
#define REG_FAR         0x70 /* on page 22-37 - RW - Default 0x0        - FIFO Access Register */
#define REG_TFR         0x74 /* on page 22-38 - RO - Default 0x0        - Transmit FIFO Read Register */
#define REG_RFW         0x78 /* on page 22-39 - WO - Default 0x0        - Receive FIFO Write */
#define REG_USR         0x7C /* on page 22-40 - RO - Default 0x6        - UART Status Register */
#define REG_TFL         0x80 /* on page 22-42 - RO - Default 0x0        - Transmit FIFO Level */
#define REG_RFL         0x84 /* on page 22-42 - RO - Default 0x0        - Receive FIFO Level Write */
#define REG_SRR         0x88 /* on page 22-43 - WO - Default 0x0        - Software Reset Register */
#define REG_SRTS        0x8C /* on page 22-44 - RW - Default 0x0        - Shadow Request to Send */
#define REG_SBCR        0x90 /* on page 22-45 - RW - Default 0x0        - Shadow Break Control Register */
#define REG_SDMAM       0x94 /* on page 22-46 - RW - Default 0x0        - Shadow DMA Mode */
#define REG_SFE         0x98 /* on page 22-47 - RW - Default 0x0        - Shadow FIFO Enable */
#define REG_SRT         0x9C /* on page 22-48 - RW - Default 0x0        - Shadow Rx Trigger */
#define REG_STET        0xA0 /* on page 22-49 - RW - Default 0x0        - Shadow Tx Empty Trigger */
#define REG_HTX         0xA4 /* on page 22-50 - RW - Default 0x0        - Halt Tx */
#define REG_DMASA       0xA8 /* on page 22-51 - WO - Default 0x0        - DMA Software Acknowledge */
#define REG_CPR         0xF4 /* on page 22-52 - RO - Default 0x373F32   - Component Parameter Register */
#define REG_UCV         0xF8 /* on page 22-54 - RO - Default 0x3331312A - Component Version */
#define REG_CTR         0xFC /* on page 22-55 - RO - Default 0x44570110 - Component Type Register */

// ACCESS MACROS
#define UART_REG(_x_)   *(volatile uint32_t *)(UART_BASE_ADD + _x_) /* _x_ is the offset */

// Define bits usage
#define BITS_DLAB  0x00000080 /* REG : LCR - Divisor Latch Access Bit - Bit 7 */
#define BITS_DLL   0x000000FF /* REG : RBR_THR_DLL - Divisor Latch Low bytes- Bits 7..0 */
#define BITS_DLH   0x000000FF /* REG : IER_DLH - Divisor Latch High bytes - Bits 7..0 */
#define BITS_FIFOE 0x00000001 /* REG : FCR - Fifo enable - Bit 0 */
#define BITS_PEN   0x00000008 /* REG : LCR - Parity enable - Bit 3 */
#define BITS_STOP  0x00000004 /* REG : LCR - Stop bits 0x0 = 1bit / 0x1 = 1.5bit - Bit 2 */
#define BITS_DLS   0x00000003 /* REG : LCR - Data Length Select. Value : 0x0=5bits / 0x1=6bits / 0x2=7bits / 0x3=8bits - Bits 1..0 */
#define BITS_DLS_8 0x00000003 /* REG : LCR - Data Length Select. Value : 0x3=8bits - Bits 1..0 */
#define BITS_TEMT  0x00000040 /* REG : LSR - If FIFO enabled (FCR[0] set to 1) : 1 = Transmitter Shift Register and FIFO both empty; else : 1 = Transmitter Holding Register and Transmitter Shift Register are both empty. - Bit 6 */

#define BITS_TX_HOLD_EMPTY 0x80


// SETTING CONSTANTS
#define BAUDRATE_CLK_DIV 0x028B /* 100000000Hz/9600baud/16(clk divider multiplier) = 651 */

#define UART0_IRQ_NO     194    /* UART0 uart0_IRQ - Level */

#define MAX_SEND         128    /* Max number of chars to send in one go */


// SYSMGR
#define SYSMGR_BASE_ADD  0xFFD08000 /* p. 2-19 */

#define REG_GENERALIO14  0x04B8     /* uart0_tx Mux Selection Register */
#define REG_UART0USEFPGA 0x06F4     /* PAS DANS LA DOC DE CYBEARLEARN !!!! MERCI !!!! (0 = HPS / 1 = FPGA) */

// ACCESS MACROS
#define SYSMGR_REG(_x_)  *(volatile uint32_t *)(UART_BASE_ADD + _x_) /* _x_ is the offset */

#define BITS_SEL_UART    0x00000003 /* REG : GENERALIO14  - UART0.TX */
#define BITS_FPGA        0x00000001 /* REG : UART0USEFPGA - Use by FPGA */

//***********************************//
//***** Global usage functions ******//

/// @brief Does all the init and config of UART0
void initUart0();

/// @brief Sends the string over via UART0
/// @param str the string to send
/// @return 1 or ERROR code on error, 0 otherwise
int sendUart0(const char* str);

#endif /* __UART_H__ */
