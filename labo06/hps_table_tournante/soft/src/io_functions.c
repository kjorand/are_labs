/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : io_functions.c
 * Author               : Leupi & Jorand
 * Date                 : 06.11.2023
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: io functions
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 0.0    06.11.2023  RLI & KJD    Initial version.
 *
*****************************************************************************************/

#include <inttypes.h>
#include "axi_lw.h"
#include "io_functions.h"
#include "table_tournante.h"
#include "hex_val.h"
#include <strings.h>

#define GET_SHIFT_FROM_REG(_REG_, _BITS_) ((TABLE_REG(_REG_) & (_BITS_)) >> (ffs(_BITS_)-1))

//***********************************//
//****** Local usage functions ******//

/// @brief Puts out a value on the lower 7-segs (should already be encoded)
/// @param value the encoded value to put on the 4 lower 7-segs
void seg7_write_3_0(uint32_t value);

/// @brief Puts out a value on the higher 2 7-segs (should already be encoded)
/// @param value the encoded value to put on the 2 higher 7-segs
void seg7_write_5_4(uint32_t value);

/// @brief Write only on seg 4
/// @param value the value to put
void seg7_write_4(uint8_t value);

/// @brief Write only on seg 5
/// @param value the value to put
void seg7_write_5(uint8_t value);


//***********************************//
//****** Functions definitions ******//

void read_key(uint8_t *state, uint8_t *rising_edge) {
    static uint8_t lastValue = 0x0;
    uint8_t currentValue = (TABLE_REG(REG_BUTTONS) & KEY_BITS);
    if(state){
        *state = currentValue;
    }
    if(rising_edge){
        *rising_edge = (~lastValue) & currentValue; // forces last = 0 and current = 1
    }

    lastValue = currentValue;
}

uint16_t read_switch(){
    return (uint16_t)(TABLE_REG(REG_SWITCHS) & SWITCHS_BITS);
}

void set_leds(uint16_t leds){
    TABLE_REG(REG_LEDS) = (TABLE_REG(REG_LEDS) & ~LEDS_BITS) | (leds & LEDS_BITS);
}

void set_indiv_leds(uint16_t mask, uint8_t val){
    if(val == LED_ON){
        TABLE_REG(REG_LEDS) |= mask & LEDS_BITS;
    }else if(val == LED_OFF){
        TABLE_REG(REG_LEDS) &= ~(mask & LEDS_BITS);
    }else if(val == LED_TOGGLE){
        TABLE_REG(REG_LEDS) ^= mask & LEDS_BITS;
    }
}

uint32_t read_interface_ID(){
    return TABLE_REG(REG_INTERFACE_ID);
}

uint32_t read_axi_lw_ID(){
    return AXI_LW_REG(REG_AXI_LW_ID);
}

void seg7_write_3_0(uint32_t value){
    TABLE_REG(REG_HEX3_0) = BITS_HEX0_3 & ~(value & BITS_HEX0_3);
}

void seg7_write_5_4(uint32_t value){
    TABLE_REG(REG_HEX5_4) = BITS_HEX4_5 & ~(value & BITS_HEX4_5);
}

void seg7_write_4(uint8_t value){
    TABLE_REG(REG_HEX5_4) = BITS_HEX4_5 & (~(value & BITS_HEX_4) | (TABLE_REG(REG_HEX5_4) & BITS_HEX_5));
}

void seg7_write_5(uint8_t value){
    TABLE_REG(REG_HEX5_4) = BITS_HEX4_5 & (( ( ~((uint16_t)value) << HEX_BITS_OFFSET) & BITS_HEX_5) | (TABLE_REG(REG_HEX5_4) & BITS_HEX_4));
}

void seg7_write_int(uint32_t value){
    uint32_t hex = 0x0; /* All off */
    for(int i = 0; i < 5; ++i){ // Only on segs 4..0
        hex |= ((uint32_t)VAL2SEG[value % 10]) << (HEX_BITS_OFFSET * (i%4) );
        value /= 10;
        if(i == 3){
            seg7_write_3_0(hex);
            hex = 0x0;
        }
    }
    seg7_write_4(hex);
}

void seg7_write_lim(lim_t lim){
    if(lim == LIM_MIN)
        seg7_write_5(CHAR_LIM_MIN);
    else if(lim == LIM_MAX)
        seg7_write_5(CHAR_LIM_MAX);
    else if(lim == LIM_NONE)
        seg7_write_5(0x0);
    // Else : error
}

void seg7_off(){
    seg7_write_3_0(0x0000);
    seg7_write_5_4(0x0000);
}

void write_target(uint16_t value){
    TABLE_REG(REG_TARGET) = value;
}

uint16_t read_target(){
    return (uint16_t)TABLE_REG(REG_TARGET);
}

uint16_t read_position(){
    return (uint16_t)TABLE_REG(REG_POSITION);
}

uint16_t read_idx(){
    return (uint16_t)TABLE_REG(REG_IDX);
}

void write_speed(uint8_t value){
    TABLE_REG(REG_SPEED) = value & BITS_SPEED;
}

uint8_t read_speed(){
    return (uint8_t)TABLE_REG(REG_SPEED) & BITS_SPEED;
}

void seq_init(){
    TABLE_REG(REG_COMANDS_AND_STATE) |= BITS_SEQ_INIT;
}

void seq_cal(){
    TABLE_REG(REG_COMANDS_AND_STATE) |= BITS_SEQ_CAL;
}

void man_enable(uint8_t dir){
    TABLE_REG(REG_COMANDS_AND_STATE) |= (uint32_t)(dir & 0x3) << MAN_BITS_OFFSET;
}

void man_disable(uint8_t dir){
    TABLE_REG(REG_COMANDS_AND_STATE) &= ~((uint32_t)(dir & 0x3) << MAN_BITS_OFFSET);
}

void enable_interrupt(){
    TABLE_REG(REG_COMANDS_AND_STATE) |= BITS_INT_ENABLE;
}

void int_ack(){
    TABLE_REG(REG_COMANDS_AND_STATE) |= BITS_INT_ACK;
}

uint32_t get_state(){
    return TABLE_REG(REG_COMANDS_AND_STATE);
}

uint8_t is_busy(){
    return (get_state() & BITS_BUSY) > 0;
}
