/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : hps_application.c
 * Author               :
 * Date                 :
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: Conception d'une interface pour la commande de la table tournante avec la carte DE1-SoC
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 *
 *
*****************************************************************************************/
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include "axi_lw.h"
#include "uart.h"
#include "io_functions.h"
#include "table_tournante.h"
#include <strings.h>
#include "interrupts.h"

#define MAN_EN      0x1
#define FLAG_DIR_1  0x2
#define INCR_SIZE   1000

#define LIM_INCR    1000
#define LIM_SPEED   0x1

#define BUFF_SIZE 100

#define SWITCHS8_5  (SWITCH_5 | SWITCH_6 | SWITCH_7 | SWITCH_8)

int __auto_semihosting;

// Only for that file
static uint32_t limit_flags;

void fpga_ISR(void){ // Used in exceptions.c
    // Retrieve interrupt type
    limit_flags = get_state() & (BITS_LIM_HIGH | BITS_LIM_LOW);

    // Ack interrupt
    int_ack();

    // Led toggle
    set_indiv_leds(LED_7, LED_TOGGLE);

    // Put out on seg
    if(limit_flags & BITS_LIM_HIGH){
        seg7_write_lim(LIM_MAX);
    }else if(limit_flags & BITS_LIM_LOW){
        seg7_write_lim(LIM_MIN);
    }

    // FIXME : only debug!
    printf("Interrupt !\n");
}

int main(void){

    printf("Laboratoire: Commande Table tournante \n");

    // DISABLE because VHDL
    // Interrupt_init();

    initUart0();

    // Read and print constants / ID
    printf("AXI ID : 0x%"PRIX32" \n", read_axi_lw_ID());
    printf("AVALON ID : 0x%"PRIX32" \n", read_interface_ID());

    // Turn all DE1-SoC leds off
    set_leds(0x0000);
    seg7_off();

    sendUart0("Hello and welcome to \"table tournante\", a program by :\r\n");
    sendUart0("Rachel Leupi and Kévin Jorand\r\n\r\n");
    sendUart0("You can use the following possibilities :\r\n");
    sendUart0(" - Une pression sur KEY0 :\r\n");
    sendUart0("     - si SW0 = '0' : Débuter la séquence de calibration de la position initiale.\r\n");
    sendUart0("     - si SW0 = '1' : Débuter la séquence d'initialisation (prise d'index).\r\n");
    sendUart0(" - Une pression sur KEY1 :\r\n");
    sendUart0("     - si SW1 = '1' : Effectuer un déplacement automatique (Une pression).\r\n");
    sendUart0("     - si SW1 = '0' : Effectuer un déplacement manuel (Pression continue).\r\n");
    sendUart0(" - Une  pression  sur  KEY2  (en  continue)  affiche  la  valeur  de  la  position  initiale mémorisée sur les 5 afficheurs 7 seg (HEX4 à HEX0) ainsi qu'un message avec la valeur de la position initiale sur le terminal UART.\r\n\r\n");
    sendUart0(" - L'état de SW2 permet de définir la direction de déplacement de la table :\r\n");
    sendUart0("     - SW2 = '0' : Déplacement positif, sens horaire.\r\n");
    sendUart0("     - SW2 = '1' : Déplacement négatif, sens anti-horaire.\r\n");
    sendUart0(" - L'état de SW4-3 permet de définir la vitesse de déplacement de la table :\r\n");
    sendUart0("     - \"00\" : Petite vitesse = 10 pas par seconde\r\n");
    sendUart0("     - \"01\" : Moyenne vitesse = 25 pas par seconde\r\n");
    sendUart0("     - \"10\" : Grande vitesse = 50 pas par seconde\r\n");
    sendUart0("     - \"11\" : Très grande vitesse = 100 pas par seconde\r\n");
    sendUart0(" - L'état de SW8-5 permet de définir le nombre de step X de 1000 incrément lors d'un déplacement automatique. La valeur X varie de 0 à 15\r\n\r\n");
    sendUart0("Note sur les leds :\r\n");
    sendUart0(" - Led 0 : Allumé si la calibration de la position initiale a été effectué.\r\n");
    sendUart0(" - Led 1 : Allumé lorsqu'un déplacement automatique est actif.\r\n");
    sendUart0(" - Led 2 : Allumé lorsqu'un déplacement manuel est actif.\r\n");
    sendUart0(" - Led 3 : Allumé lorsqu'un déplacement de sortie de fin de course est actif (à la suite d'une interruption).\r\n");
    sendUart0(" - Led 7 : Toggle lors d'une nouvelle interruption.\r\n");
    sendUart0(" - Les 5 afficheurs 7 seg (HEX4 à HEX0) affichent la valeur de la position actuelle de la table.\r\n");
    sendUart0(" - L'afficheur 7 seg HEX5 sera utilisé pour indiquer le dépassement des fins de courses\r\n\r\n\r\n");

    limit_flags = 0;
    // DISABLE BECAUSE VHDL
    // enable_interrupt();

    // END INIT !

    uint8_t keys;
    uint8_t keys_pressed;
    uint16_t switchs;
    uint8_t nb_incr = 0;
    int16_t increment = INCR_SIZE;
    uint16_t idx = 0;
    char buff[BUFF_SIZE + 1]; // Taking termination into account
    uint8_t man_flags = 0x0;
    while(1){
        // Read keys
        read_key(&keys, &keys_pressed); // Love it when you can just change 1 var and your prog behaviour changes and complies with assistant expectations !!

        // Read switchs
        switchs = read_switch();

        if(limit_flags){
            // Ensure automatic : stop
            nb_incr = 0;
            set_indiv_leds(LED_2, LED_OFF);

            // ensures man off
            man_disable(DIR_0 | DIR_1);
            set_indiv_leds(LED_2, LED_OFF);
            man_flags = 0x0; // RESET

            // Does comeback
            if(!is_busy()){ // MSS sequences are non-interruptible
                set_indiv_leds(LED_3, LED_ON);
                uint16_t pos = read_position();
                if(pos < MAX_POS && pos > MIN_POS){ // Already came back
                    limit_flags = 0x0;
                }else{ // We have hand and can instruct to come back !
                    write_speed(LIM_SPEED);
                    if(limit_flags & BITS_LIM_HIGH){
                        write_target(MAX_POS - LIM_INCR);
                    }else if(limit_flags & BITS_LIM_LOW){
                        write_target(MIN_POS + LIM_INCR);
                    }
                }
            }
        }else{
            set_indiv_leds(LED_3, LED_OFF);
            seg7_write_lim(LIM_NONE);
            if(keys_pressed & KEY0){ // Key 0 pressed
                if(switchs & SWITCH_0){
                    sendUart0("Start initialisation\r\n");
                    seq_init();
                }else{
                    sendUart0("Start calibration\r\n");
                    seq_cal();
                    set_indiv_leds(LED_0, LED_ON);
                }
            }

            if((keys_pressed & KEY1) && (switchs & SWITCH_1) && !is_busy()){ // Key 1 pressed
                // Auto move
                nb_incr = (switchs & (SWITCHS8_5)) >> (ffs(SWITCHS8_5) - 1);
                if(switchs & SWITCH_2){ // Direction selection
                    increment = INCR_SIZE;
                }else{
                    increment = -INCR_SIZE;
                }
                set_indiv_leds(LED_2, LED_ON);
            }
            if(nb_incr > 0 && !is_busy()){
                snprintf((char *)&buff, BUFF_SIZE, "Doing step : %"PRIu8"\r\n", nb_incr);
                sendUart0(buff);
                --nb_incr;
                write_target((int32_t)read_target()+increment);
            }else if(nb_incr == 0){
                set_indiv_leds(LED_2, LED_OFF);
            }

            if((keys & KEY1) && !(switchs & SWITCH_1) && !is_busy()){ // Key 1 held
                // Manual move
                if(switchs & SWITCH_2){ // Direction selection
                    if(man_flags != (MAN_EN | FLAG_DIR_1)){
                        man_disable(DIR_0);
                        man_enable(DIR_1);
                        man_flags = (MAN_EN | FLAG_DIR_1);
                    }
                }else{
                    if(man_flags != (MAN_EN)){
                        man_disable(DIR_1);
                        man_enable(DIR_0);
                        man_flags = MAN_EN;
                    }
                }
                set_indiv_leds(LED_2, LED_ON);
            }else if(!(keys & KEY1) || (switchs & SWITCH_1)){
                man_disable(DIR_0 | DIR_1);
                set_indiv_leds(LED_2, LED_OFF);
                man_flags = 0x0; // RESET
            }
        }

        if(keys & KEY2){
            idx = read_idx();
            seg7_write_int(idx);
        }else{
            seg7_write_int(read_position());
        }
        if(keys_pressed & KEY2){
            snprintf((char *)&buff, BUFF_SIZE, "Position enregistrée : %"PRIu16"\r\n", idx);
            sendUart0(buff);
        }

        write_speed((switchs & (SWITCH_3 | SWITCH_4)) >> (ffs(SWITCH_3) - 1));
    }


}

