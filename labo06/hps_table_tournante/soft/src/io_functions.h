/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : io_functions.h
 * Author               : Leupi & Jorand
 * Date                 : 06.11.2023
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: Header file for io functions
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 0.0    06.11.2023  RLI & KJD    Initial version.
 *
*****************************************************************************************/

#ifndef __IO_FUNCTIONS_H__
#define __IO_FUNCTIONS_H__

#include <stdint.h>

#define LED_0       0x01
#define LED_1       0x02
#define LED_2       0x04
#define LED_3       0x08
#define LED_4       0x10
#define LED_5       0x20
#define LED_6       0x40
#define LED_7       0x80

#define LED_ON      0x01
#define LED_OFF     0x00
#define LED_TOGGLE  0x02

#define SWITCH_0    0x0001
#define SWITCH_1    0x0002
#define SWITCH_2    0x0004
#define SWITCH_3    0x0008
#define SWITCH_4    0x0010
#define SWITCH_5    0x0020
#define SWITCH_6    0x0040
#define SWITCH_7    0x0080
#define SWITCH_8    0x0100
#define SWITCH_9    0x0200

#define DIR_0       0x1
#define DIR_1       0x2

//***********************************//
//******* Global usage types ********//

typedef enum {LIM_MIN, LIM_MAX, LIM_NONE} lim_t;


//***********************************//
//****** Global usage function ******//

/// @brief Reads the keys register and fills the state and edge passed in parameter as long as not null.
/// Information is represented as bitfield : each bit representing one key
/// @param state receives the current state of the buttons
/// @param rising_edge  receives the rising edge of the buttons : every button which saw a rising edge since last call is set to '1' others to '0'
void read_key(uint8_t *state, uint8_t *rising_edge);

/// @brief Reads the value on the switches
/// @return The state of the switches, one bit for each
uint16_t read_switch();

/// @brief Puts the argument on the leds (each led representing one bit for the LSBs)
/// @param leds The value to put on leds
void set_leds(uint16_t leds);

/// @brief Puts the value '0' or '1' or toggles it on all bits of mask. It is advised to use LED_0 .. LED_7 (or'able) values as mask
/// @param mask the bits (leds) to chagne
/// @param val LED_ON or LED_OFF or LED_TOGGLE
void set_indiv_leds(uint16_t mask, uint8_t val);

/// @brief Reads the avalon interface ID constant
/// @return The interface avalon ID constant
uint32_t read_interface_ID();

/// @brief Reads the AXI lightweight bus constant ID
/// @return The AXI lightweight bus constant ID
uint32_t read_axi_lw_ID();

/// @brief Writes a positive integer value to the hex displays
/// @param value the value to be shown
void seg7_write_int(uint32_t value);

/// @brief Shows just the lower or higher limit's symbol or cleans it
/// @param lim the limit to show
void seg7_write_lim(lim_t lim);

/// @brief Turns off all 7 segments
void seg7_off();

/// @brief Write the target position
/// @param value the value to write as target position
void write_target(uint16_t value);

/// @brief Reads target register
/// @return the value of the read target register
uint16_t read_target();

/// @brief Reads position register
/// @return the value of the read position register
uint16_t read_position();

/// @brief Reads idx register
/// @return the value of the read idx register
uint16_t read_idx();

/// @brief Write the speed position
/// @param value the value to write as speed position
///    - "00" : 10 Hz
///    - "01" : 25 Hz
///    - "10" : 50 Hz
///    - "11" : 100 Hz
void write_speed(uint8_t value);

/// @brief Reads speed register
/// @return the value of the read speed register
///    - "00" : 10 Hz
///    - "01" : 25 Hz
///    - "10" : 50 Hz
///    - "11" : 100 Hz
uint8_t read_speed();

/// @brief Launch initialisation sequence
void seq_init();

/// @brief Launch calibration sequence
void seq_cal();

/// @brief Enables manual mode in direction dir
/// @param dir bits [1..0] choses direction ([1] dir_1 / [0] dir_0), others unused
void man_enable(uint8_t dir);

/// @brief Disables manual mode in direction dir
/// @param dir bits [1..0] choses direction ([1] dir_1 / [0] dir_0), others unused
void man_disable(uint8_t dir);

/// @brief Enables interupt on fpga
void enable_interrupt();

/// @brief Acknoledges the interrupt on fpga
void int_ack();

/// @brief Returns the full command and state register
/// @return the full command and state register
uint32_t get_state();

/// @brief Checks if the fpga currently is busy
/// @return true if the fpga is busy
uint8_t is_busy();

#endif /* __IO_FUNCTIONS_H__ */
