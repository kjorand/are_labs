#ifndef __TABLE_TOURNANTE_H__
#define __TABLE_TOURNANTE_H__

// Base address
#define TABLE_BASE_ADD   0x00010000 /* byte (or CPU) address */

// ACCESS MACROS
#define TABLE_REG(_x_)   *(volatile uint32_t *)(AXI_LW_HPS_FPGA_BASE_ADD + TABLE_BASE_ADD + _x_) // _x_ is an offset with respect to the base address

// Registers
#define REG_INTERFACE_ID        0x00 /* Avalon: 0x00 - | [31..0] Constante ID interface                                                                                                      | not used */
#define REG_BUTTONS             0x04 /* Avalon: 0x01 - | [31..4] "0..0" / [3..0] buttons                                                                                                     | not used */
#define REG_SWITCHS             0x08 /* Avalon: 0x02 - | [31..10] "0..0" / [9..0] switchs                                                                                                    | not used */
#define REG_LEDS                0x0C /* Avalon: 0x03 - | [31..8] "0..0" / [7..0] leds                                                                                                        | [31..8] reserved / [7..0] leds */
#define REG_HEX3_0              0x10 /* Avalon: 0x04 - | [31..28] "0..0" / [27..21] hex3 / [20..14] hex2 / [13..7] hex1 / [6..0] hex0                                                        | [31..28] reserved / [27..21] hex3 / [20..14] hex2 / [13..7] hex1 / [6..0] hex0 */
#define REG_HEX5_4              0x14 /* Avalon: 0x05 - | [31..14] "0..0" / [13..7] hex5 / [6..0] hex4                                                                                        | [31..14] reserved / [13..7] hex5 / [6..0] hex4 */
#define REG_SPEED               0x18 /* Avalon: 0x06 - | [31..2] "0..0" / [1..0] speed                                                                                                       | [31..2] reserved / [1..0] speed */
#define REG_COMANDS_AND_STATE   0x1C /* Avalon: 0x07 - | [31] limit_high / [30] limit_low / [29..6] "0..0" / [5] man_dir_1 / [4] man_dir_0 / [3] interrupt_enable / [2] busy / [1..0] "0..0" | [31..30] ack_interrupt / [29..6] reserved / [5] man_dir_1 / [4] man_dir_0 / [3] interrupt_enable / [2] reserved / [1] seq_init / [0] seq_cal */
#define REG_TARGET              0x20 /* Avalon: 0x08 - | [31..16] "0..0" / [15..0] target                                                                                                    | [31..16] reserved / [15..0] target */
#define REG_POSITION            0x24 /* Avalon: 0x09 - | [31..16] "0..0" / [15..0] position                                                                                                  | [31..0] reserved */
#define REG_IDX                 0x28 /* Avalon: 0x0A - | [31..16] "0..0" / [15..0] idx                                                                                                       | [31..0] reserved */

// Define bits usage
#define SWITCHS_BITS    0x000003FF
#define LEDS_BITS       0x000000FF
#define KEY_BITS        0x0000000F

#define MAX_HEX         6

#define HEX0_BITS       0x0000007F
#define HEX_BITS_OFFSET 7 /* Shift from one 7-seg to the next */

#define MAN_BITS_OFFSET 4 /* Shift from lsb to man bits */

#define BITS_SPEED      0x00000003 /* REG : SPEED / Bits [1..0] */
#define BITS_INT_ACK    0xC0000000 /* REG : COMANDS_AND_STATE / Bits [31..30] */
#define BITS_LIM_HIGH   0x80000000 /* REG : COMANDS_AND_STATE / Bits [31] */
#define BITS_LIM_LOW    0x40000000 /* REG : COMANDS_AND_STATE / Bits [30] */
#define BITS_MAN_DIR_1  0x00000020 /* REG : COMANDS_AND_STATE / Bits [5] */
#define BITS_MAN_DIR_0  0x00000010 /* REG : COMANDS_AND_STATE / Bits [4] */
#define BITS_INT_ENABLE 0x00000008 /* REG : COMANDS_AND_STATE / Bits [3] */
#define BITS_BUSY       0x00000004 /* REG : COMANDS_AND_STATE / Bits [2] */
#define BITS_SEQ_INIT   0x00000002 /* REG : COMANDS_AND_STATE / Bits [1] */
#define BITS_SEQ_CAL    0x00000001 /* REG : COMANDS_AND_STATE / Bits [0] */
#define BITS_HEX0_3		0x0FFFFFFF /* REG : REG_HEX3_0 / Bits [27..0] */
#define BITS_HEX4_5		0x00003FFF /* REG : REG_HEX5_4 / Bits [13..0] */

#define BITS_HEX_4      0x0000007F /* REG : REG_HEX5_4 / Bits [6..0] */
#define BITS_HEX_5      0x00003F80 /* REG : REG_HEX5_4 / Bits [13..7] */

// Individual key's bits
#define KEY0    0x1
#define KEY1    0x2
#define KEY2    0x4
#define KEY3    0x8

// Define values for the parameters ...
#define FREQ_10H        0 /* "00" : 10 Hz */
#define FREQ_25H        1 /* "01" : 25 Hz */
#define FREQ_50H        2 /* "10" : 50 Hz */
#define FREQ_100H       3 /* "11" : 100 Hz */


// Sepcial chars on segs
#define CHAR_LIM_MIN    0x48 /* Midle and bottom segment */
#define CHAR_LIM_MAX    0x41 /* Top and midle segment */

// Constants
#define MIN_POS         0x09C4 /* from assignment 2500 */
#define MAX_POS         0xF424 /* from assignment 62500 */

#endif /* __TABLE_TOURNANTE_H__ */
