------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : avl_user_interface.vhd
-- Author               :
-- Date                 : 04.08.2022
--
-- Context              : Avalon user interface
--
------------------------------------------------------------------------------------------
-- Description :
--
------------------------------------------------------------------------------------------
-- Dependencies :
--
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer    Comments
-- 0.0    See header              Initial version

------------------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity avl_user_interface is
  port(
    -- Avalon bus
    avl_clk_i           : in  std_logic;
    avl_reset_i         : in  std_logic;
    avl_address_i       : in  std_logic_vector(13 downto 0);
    avl_byteenable_i    : in  std_logic_vector(3 downto 0); -- ne pas en tenir compte ACS
    avl_write_i         : in  std_logic;
    avl_writedata_i     : in  std_logic_vector(31 downto 0);
    avl_read_i          : in  std_logic;
    avl_readdatavalid_o : out std_logic;
    avl_readdata_o      : out std_logic_vector(31 downto 0);
    avl_waitrequest_o   : out std_logic;
    avl_irq_o           : out std_logic;
    -- User interface
    boutton_i           : in  std_logic_vector(3 downto 0);
    switch_i            : in  std_logic_vector(9 downto 0);
    led_o               : out std_logic_vector(7 downto 0);
    hex0_o              : out std_logic_vector(6 downto 0);
    hex1_o              : out std_logic_vector(6 downto 0);
    hex2_o              : out std_logic_vector(6 downto 0);
    hex3_o              : out std_logic_vector(6 downto 0);
    hex4_o              : out std_logic_vector(6 downto 0);
    hex5_o              : out std_logic_vector(6 downto 0);
    -- table tournante
    t_idx_i             : in  std_logic;
    t_capt_sup_i        : in  std_logic;
    t_inc_pos_i         : in  std_logic;
    t_dec_pos_i         : in  std_logic;
    t_position_o        : out std_logic_vector(15 downto 0);
    t_dir_pap_o         : out std_logic;
    t_enable_pap_o      : out std_logic;
    t_top_pap_o         : out std_logic
  );
end avl_user_interface;

architecture rtl of avl_user_interface is

    --| Components declaration |--------------------------------------------------------------
    component gestion_IO
    port(
        clk_i               : in  std_logic;
        reset_i             : in  std_logic;

        button_i            : in  std_logic_vector(3 downto 0);
        switch_i            : in  std_logic_vector(9 downto 0);
        led_i               : in  std_logic_vector(7 downto 0);
        hex_low_i           : in  std_logic_vector(27 downto 0);
        hex_high_i          : in  std_logic_vector(13 downto 0);

        cs_led_wr_i         : in  std_logic;
        cs_hex_wr_high_i    : in  std_logic;
        cs_hex_wr_low_i     : in  std_logic;

        button_sync_o       : out std_logic_vector(3 downto 0);
        switch_sync_o       : out std_logic_vector(9 downto 0);
        led_o               : out std_logic_vector(7 downto 0);

        hex0_o              : out std_logic_vector(6 downto 0);
        hex1_o              : out std_logic_vector(6 downto 0);
        hex2_o              : out std_logic_vector(6 downto 0);
        hex3_o              : out std_logic_vector(6 downto 0);
        hex4_o              : out std_logic_vector(6 downto 0);
        hex5_o              : out std_logic_vector(6 downto 0)
    );
    end component;

    component rising_edge_detector
    port(
        clk_i               : in  std_logic;
        reset_i             : in  std_logic;

        etat_i              : in std_logic;
        etat_o              : out std_logic

    );
    end component;


    component top_gen
    port(
        clk_i               : in  std_logic;
        reset_i             : in  std_logic;
        speed_i             : in  std_logic_vector(1 downto 0);

        top_o               : out std_logic
    );
    end component;

    component counter
    port(
        clk_i               : in  std_logic;
        reset_i             : in  std_logic;
        inc_i               : in  std_logic;
        dec_i               : in  std_logic;
        load_i              : in  std_logic;
        value_i             : in  std_logic_vector(15 downto 0);

        counter_o           : out std_logic_vector(15 downto 0)
    );
    end component;

    component mss
    port(
        clk_i               : in  std_logic;
        reset_i             : in  std_logic;
        cmd_cal_i           : in  std_logic;
        idx_i               : in  std_logic;
        optic_i             : in  std_logic;
        cmd_init_i          : in  std_logic;
        to_target_i         : in  std_logic;
        target_reached_i    : in  std_logic;
        man_dir_1_i         : in  std_logic;
        man_dir_0_i         : in  std_logic;

        init_pos_o          : out std_logic;
        busy_o              : out std_logic;
        force_dir_0_o       : out std_logic;
        force_dir_1_o       : out std_logic;
        force_low_speed_o   : out std_logic;
        run_o               : out std_logic;
        save_idx_o          : out std_logic;
        retrieve_idx_o      : out std_logic;
        init_target_o       : out std_logic
    );
    end component;

    --| Constants declarations |--------------------------------------------------------------

    constant HORS_ZONE      : std_logic_vector(31 downto 0) := x"CAFEFECA";
    constant AVALON_ID      : std_logic_vector(31 downto 0) := x"BEEFBEEF";
    constant RESERVED       : std_logic_vector(31 downto 0) := x"EEEEEEBA";
    constant UNUSED         : std_logic_vector(31 downto 0) := x"DEADCAFE";
    constant INITIAL_POS    : std_logic_vector(15 downto 0) := x"7530"; -- 30000
    constant INITIAL_TARGET : std_logic_vector(15 downto 0) := x"C355";  -- 50005
    constant INITIAL_IDX_POS: std_logic_vector(15 downto 0) := x"9C40";  -- 40000
    constant MIN            : unsigned(15 downto 0)         := x"09C4"; -- 2500
    constant MAX            : unsigned(15 downto 0)         := x"F424"; -- 62500

    --| Signals declarations   |--------------------------------------------------------------

    -- IO signals
    signal button_s         : std_logic_vector(3 downto 0);
    signal switch_s         : std_logic_vector(9 downto 0);
    signal led_s            : std_logic_vector(7 downto 0);
    signal hex0_s           : std_logic_vector(6 downto 0);
    signal hex1_s           : std_logic_vector(6 downto 0);
    signal hex2_s           : std_logic_vector(6 downto 0);
    signal hex3_s           : std_logic_vector(6 downto 0);
    signal hex4_s           : std_logic_vector(6 downto 0);
    signal hex5_s           : std_logic_vector(6 downto 0);

    -- Write chips select
    signal cs_led_wr_s      : std_logic;
    signal cs_hex_wr_low_s  : std_logic;
    signal cs_hex_wr_high_s : std_logic;
    signal cs_speed_s       : std_logic;
    signal cs_target_wr_s   : std_logic;
    signal cs_cmd_state_s   : std_logic;

    -- Top gen signals
    signal speed_s          : std_logic_vector(1 downto 0);
    signal speed_top_gen_s  : std_logic_vector(1 downto 0);
    signal top_s            : std_logic;

    -- Target_s management signals
    signal target_s         : std_logic_vector(15 downto 0);
    signal target_saved_s   : std_logic_vector(15 downto 0);
    signal target_reached_s : std_logic;

    -- counter management signals
    signal load_cond_cnt_s  : std_logic;
    signal pos_to_load_s    : std_logic_vector(15 downto 0);
    signal position_s       : std_logic_vector(15 downto 0);

    -- direction & index
    signal dir_s            : std_logic;
    signal idx_pos_s        : std_logic_vector(15 downto 0);

    -- IRQ's signal & cmd_state reg
    signal over_max_s       : std_logic;
    signal under_min_s      : std_logic;
    signal load_over_max_s  : std_logic;
    signal load_under_min_s : std_logic;
    signal under_min_rising_edge_s : std_logic;
    signal over_max_rising_edge_s  : std_logic;
    signal irq_ack_s        : std_logic;
    signal limite_high_s    : std_logic;
    signal limite_low_s     : std_logic;
    signal irq_en_s         : std_logic;

    signal cmd_cal_s        : std_logic;
    signal cmd_init_s       : std_logic;
    signal man_dir_1_s      : std_logic;
    signal man_dir_0_s      : std_logic;

    -- MSS output signals
    signal force_low_speed_s: std_logic;
    signal init_target_s    : std_logic;
    signal run_s            : std_logic;
    signal init_pos_s       : std_logic;
    signal retrieve_idx_s   : std_logic;
    signal save_idx_s       : std_logic;
    signal force_dir1_s     : std_logic;
    signal force_dir0_s     : std_logic;
    signal busy_s           : std_logic;


begin

    -- ####################
    -- Top generation
    -- ####################

    top_gen_avl_user_interface : top_gen
    port map (
        clk_i           => avl_clk_i,
        reset_i         => avl_reset_i,
        speed_i         => speed_top_gen_s,

        top_o           => top_s
    );

    t_top_pap_o <= top_s;

    -- ####################
    -- Speed register
    -- ####################

    speed : process (avl_clk_i, avl_reset_i) is
    begin

        if avl_reset_i = '1' then
            speed_s <= "00";

        elsif rising_edge(avl_clk_i) then
            if cs_speed_s = '1' then
                speed_s <= avl_writedata_i(1 downto 0);
            end if;

        end if;
    end process;

    speed_top_gen_s <= "00" when force_low_speed_s = '1' else speed_s;

    -- ####################
    -- Target register
    -- ####################

    target_s <= INITIAL_POS when init_target_s = '1' else avl_writedata_i(15 downto 0);

    Target : process (avl_clk_i, avl_reset_i) is
    begin

        if avl_reset_i = '1' then
            target_saved_s <= INITIAL_TARGET;

        elsif rising_edge(avl_clk_i) then
            if init_target_s or cs_target_wr_s then
                target_saved_s <= target_s;
            end if;

        end if;
    end process;

    -- ####################
    -- Counter and position register
    -- ####################

    counter_avl_user_interface : counter
    port map (
        clk_i               => avl_clk_i,
        reset_i             => avl_reset_i,
        inc_i               => t_inc_pos_i,
        dec_i               => t_dec_pos_i,
        load_i              => load_cond_cnt_s,
        value_i             => pos_to_load_s,

        counter_o           => position_s
    );

    load_cond_cnt_s     <= init_pos_s or retrieve_idx_s;
    pos_to_load_s       <= INITIAL_POS when init_pos_s = '1' else idx_pos_s;
    target_reached_s    <= '1' when target_saved_s = position_s else '0';

    t_position_o <= position_s;

    -- ####################
    -- Idx register
    -- ####################

    Idx : process (avl_clk_i, avl_reset_i) is
    begin

        if avl_reset_i = '1' then
            idx_pos_s <= INITIAL_IDX_POS;

        elsif rising_edge(avl_clk_i) then
            if save_idx_s then
                idx_pos_s <= position_s;
            end if;

        end if;
    end process;

    -- ####################
    -- Dir setting
    -- ####################

    dir_s <= '1'    when force_dir1_s = '1' else
             '0'    when force_dir0_s = '1' else
             '1'    when (unsigned(target_saved_s) < unsigned(position_s)) else
             '0';

    t_dir_pap_o <= dir_s;

    Dir : process (avl_clk_i, avl_reset_i) is
    begin
        if avl_reset_i = '1' then
            man_dir_0_s <= '0';
            man_dir_1_s <= '0';
        elsif rising_edge(avl_clk_i) then
            if cs_cmd_state_s = '1' then
                man_dir_0_s <= avl_writedata_i(4);
                man_dir_1_s <= avl_writedata_i(5);
            end if;
        end if;

    end process;

    -- ####################
    -- MSS
    -- ####################

    mss_avl_user_interface : mss
    port map (
        clk_i               => avl_clk_i,
        reset_i             => avl_reset_i,
        cmd_cal_i           => cmd_cal_s,
        idx_i               => t_idx_i,
        optic_i             => t_capt_sup_i,
        cmd_init_i          => cmd_init_s,
        to_target_i         => cs_target_wr_s,
        target_reached_i    => target_reached_s,
        man_dir_1_i         => man_dir_1_s,
        man_dir_0_i         => man_dir_0_s,

        init_pos_o          => init_pos_s,
        busy_o              => busy_s,
        force_dir_0_o       => force_dir0_s,
        force_dir_1_o       => force_dir1_s,
        force_low_speed_o   => force_low_speed_s,
        run_o               => run_s,
        save_idx_o          => save_idx_s,
        retrieve_idx_o      => retrieve_idx_s,
        init_target_o       => init_target_s
    );

    t_enable_pap_o <= run_s;

    -- ####################
    -- Overlimite & IRQ

    under_min_s <= '1' when  (unsigned(position_s) < MIN) else '0';
    over_max_s <= '1' when (unsigned(position_s) > MAX) else '0';

    irq_ack_s   <= '1' when cs_cmd_state_s = '1' and (avl_writedata_i(31 downto 30) = "11") else '0';
    load_under_min_s <= '1' when ( under_min_rising_edge_s = '1' or irq_ack_s = '1') else '0';
    load_over_max_s  <= '1' when ( over_max_rising_edge_s = '1' or irq_ack_s = '1') else '0';

    Limites : process (avl_clk_i, avl_reset_i) is
    begin

        if avl_reset_i = '1' then
            limite_high_s   <= '0';
            limite_low_s    <= '0';

        elsif rising_edge(avl_clk_i) then
            if load_over_max_s = '1' then
                if (irq_ack_s = '1') then
                    limite_high_s <= '0';
                else
                    limite_high_s <= '1';
                end if;
            end if;

            if load_under_min_s = '1' then
                if (irq_ack_s = '1') then
                    limite_low_s <= '0';
                else
                    limite_low_s <= '1';
                end if;
            end if;

        end if;
    end process;

    IRQ_EN_Cmd_State : process (avl_clk_i, avl_reset_i) is
    begin

        if avl_reset_i = '1' then
            irq_en_s <= '0';

        elsif rising_edge(avl_clk_i) then

            if cs_cmd_state_s then
                irq_en_s <= avl_writedata_i(3);
            end if;

        end if;
    end process;

    cmd_cal_s   <= '1' when avl_writedata_i(0) and cs_cmd_state_s else '0';
    cmd_init_s  <= '1' when avl_writedata_i(1) and cs_cmd_state_s else '0';

    --avl_irq_o <= (limite_high_s or limite_low_s) and irq_en_s;
    avl_irq_o <= '0'; -- abandon des irq manque de temps

    REDlow : rising_edge_detector
    port map (
        clk_i           => avl_clk_i,
        reset_i         => avl_reset_i,

        etat_i          => under_min_s,
        etat_o          => under_min_rising_edge_s
    );

    REDhigh : rising_edge_detector
    port map (
        clk_i           => avl_clk_i,
        reset_i         => avl_reset_i,

        etat_i          => over_max_s,
        etat_o          => over_max_rising_edge_s
    );



    -- ####################
    -- Read access part
    -- ####################

    read_channel : process (avl_clk_i, avl_reset_i) is
    begin

        if avl_reset_i = '1' then
            avl_readdatavalid_o <= '0';
            avl_readdata_o      <= (others => '0');


        elsif rising_edge(avl_clk_i) then

            if avl_read_i = '1' then

                if avl_address_i(13 downto 8) = "000000" then -- in Zone

                    avl_readdata_o <= (others => '0');

                    case avl_address_i(7 downto 0) is
                        when x"00" => -- CPU addr : 00
                            avl_readdata_o              <= AVALON_ID;
                        when x"01" => -- CPU addr : 04
                            avl_readdata_o(3 downto 0)  <= button_s;
                        when x"02" => -- CPU addr : 08
                            avl_readdata_o(9 downto 0)  <= switch_s;
                        when x"03" => -- CPU addr : 0C
                            avl_readdata_o(7 downto 0)  <= led_s;
                        when x"04" => -- CPU addr : 10
                            avl_readdata_o(27 downto 0) <= hex3_s & hex2_s & hex1_s & hex0_s;
                        when x"05" => -- CPU addr : 14
                            avl_readdata_o(13 downto 0) <= hex5_s & hex4_s;
                        when x"06" => -- CPU addr : 18
                            avl_readdata_o(1 downto 0)  <= speed_top_gen_s;
                        when x"07" => -- CPU addr : 1C
                            avl_readdata_o <= limite_high_s & limite_low_s
                                                            & "000000000000000000000000"
                                                            & man_dir_1_s & man_dir_0_s
                                                            & irq_en_s & busy_s & "00";
                        when x"08" => -- CPU addr : 20
                            avl_readdata_o(15 downto 0) <= target_saved_s;
                        when x"09" => -- CPU addr : 0x24
                            avl_readdata_o(15 downto 0) <= position_s;
                        when x"0A" => -- CPU addr : 0x28
                            avl_readdata_o(15 downto 0) <= idx_pos_s;
                        when others =>
                            avl_readdata_o              <= UNUSED;
                    end case;

                else -- Out of Zone
                    avl_readdata_o   <= HORS_ZONE;

                end if;

                avl_readdatavalid_o <= '1';

            else
                avl_readdatavalid_o <= '0';

            end if;

        end if;

    end process;

    -- ####################
    -- Write access part
    -- ####################

    write_channel : process (avl_clk_i, avl_reset_i)
    begin

        if avl_reset_i = '1' then

            cs_led_wr_s         <= '0';
            cs_hex_wr_low_s     <= '0';
            cs_hex_wr_high_s    <= '0';
            cs_target_wr_s      <= '0';
            cs_speed_s          <= '0';
            cs_cmd_state_s      <= '0';

        elsif rising_edge(avl_clk_i) then

            cs_led_wr_s      <= '0';
            cs_hex_wr_low_s  <= '0';
            cs_hex_wr_high_s <= '0';
            cs_speed_s       <= '0';
            cs_target_wr_s   <= '0';
            cs_cmd_state_s   <= '0';

            if avl_write_i = '1' then
                if avl_address_i(13 downto 8) = "000000" then
                    case avl_address_i(7 downto 0) is
                        when x"03" => -- CPU addr : 0C
                            cs_led_wr_s   <= '1';

                        when x"04" => -- CPU addr : 10
                            cs_hex_wr_low_s   <= '1';

                        when x"05" => -- CPU addr : 14
                            cs_hex_wr_high_s  <= '1';

                        when x"06" => -- CPU addr : 18
                            cs_speed_s        <= '1';

                        when x"07" => -- CPU addr : 1C
                            cs_cmd_state_s <= '1';

                        when x"08" => -- CPU addr : 20
                            cs_target_wr_s <= '1';

                        when others =>
                            -- for simulation
                    end case;
                end if;
            end if;
        end if;

    end process;

    -- ####################
    -- Interface management
    -- ####################

    avl_waitrequest_o <= '0';

    -- ####################
    -- Gestion IO DE1-SoC
    -- ####################

    gestion_IO_avl_user_interface : gestion_IO
    port map (
        clk_i         => avl_clk_i,
        reset_i       => avl_reset_i,

        button_i      => boutton_i,
        switch_i      => switch_i,
        led_i         => avl_writedata_i(7 downto 0),
        hex_low_i     => avl_writedata_i(27 downto 0),
        hex_high_i    => avl_writedata_i(13 downto 0),

        cs_led_wr_i      => cs_led_wr_s,
        cs_hex_wr_low_i  => cs_hex_wr_low_s,
        cs_hex_wr_high_i => cs_hex_wr_high_s,

        button_sync_o => button_s,
        switch_sync_o => switch_s,

        led_o         => led_s,
        hex0_o        => hex0_s,
        hex1_o        => hex1_s,
        hex2_o        => hex2_s,
        hex3_o        => hex3_s,
        hex4_o        => hex4_s,
        hex5_o        => hex5_s
    );

    led_o  <= led_s;
    hex0_o <= hex0_s;
    hex1_o <= hex1_s;
    hex2_o <= hex2_s;
    hex3_o <= hex3_s;
    hex4_o <= hex4_s;
    hex5_o <= hex5_s;

end rtl;