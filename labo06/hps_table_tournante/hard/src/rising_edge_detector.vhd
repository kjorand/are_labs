------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
--
-- File                 : rising_edge_detector.vhd
-- Author               : Rachel Leupi / Kévin Jorand
-- Date                 : 23.01.2023
--
-- Context              : edge detector
--
------------------------------------------------------------------------------------------
-- Description :
--
------------------------------------------------------------------------------------------
-- Dependencies : none
--
------------------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity rising_edge_detector is
  port(
    clk_i               : in  std_logic;
    reset_i             : in  std_logic;

    etat_i              : in std_logic;
    etat_o              : out std_logic
  );
end rising_edge_detector;

architecture rtl of rising_edge_detector is

-- Signaux
signal etat_past_s : std_logic := '0';
signal etat_pres_s : std_logic := '0';


begin
    process (clk_i, reset_i)
    begin
        if reset_i = '1' then
            etat_past_s <= '0';
            etat_pres_s <= '0';

        elsif rising_edge(clk_i) then
            etat_pres_s <= etat_i;
            etat_past_s <= etat_pres_s;
        end if;
    end process;

    etat_o <= (not etat_past_s) and etat_pres_s;

end rtl;