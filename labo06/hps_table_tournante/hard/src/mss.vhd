------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
--
-- File                 : mss.vhd
-- Author               : Rachel Leupi / Kévin Jorand
-- Date                 : 23.01.2023
--
-- Context              : Avalon user interface
--
------------------------------------------------------------------------------------------
-- Description : mss to manage the differents modes
--
------------------------------------------------------------------------------------------
-- Dependencies : none
--
------------------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity mss is
  port(
    clk_i               : in  std_logic;
    reset_i             : in  std_logic;
    cmd_cal_i           : in  std_logic;
    idx_i               : in  std_logic;
    optic_i             : in  std_logic;
    cmd_init_i          : in  std_logic;
    to_target_i         : in  std_logic;
    target_reached_i    : in  std_logic;
    man_dir_1_i         : in  std_logic;
    man_dir_0_i         : in  std_logic;

    init_pos_o          : out std_logic;
    busy_o              : out std_logic;
    force_dir_0_o       : out std_logic;
    force_dir_1_o       : out std_logic;
    force_low_speed_o   : out std_logic;
    run_o               : out std_logic;
    save_idx_o          : out std_logic;
    retrieve_idx_o      : out std_logic;
    init_target_o       : out std_logic
  );
end mss;

architecture m_etat of mss is

type t_etat is (    START,
                    START_CALIBRATION,
                    CALIBRATING_1,
                    CALIBRATING_2,
                    SET_IDX_POS,
                    SET_TARGET_INIT_POS,
                    INIT_1,
                    INIT_2,
                    GET_IDX_POS,
                    MOVE_TO_TARGET,
                    BACK_TO_HOME,
                    MAN_COUNTER_CLOCKWISE,
                    MAN_CLOCKWISE
                );

-- Signaux
signal etat_pres, etat_fut : t_etat;



begin
    Fut:process (cmd_cal_i, optic_i, idx_i, cmd_init_i, to_target_i, target_reached_i, etat_pres)
    begin

        etat_fut <= START; -- valeur par defaut

        init_pos_o          <= '0';
        busy_o              <= '0';
        force_dir_0_o       <= '0';
        force_dir_1_o       <= '0';
        force_low_speed_o   <= '0';
        run_o               <= '0';
        save_idx_o          <= '0';
        retrieve_idx_o      <= '0';
        init_target_o       <= '0';

        case etat_pres is
        when START =>
            if cmd_cal_i = '1' then
                etat_fut <= START_CALIBRATION;
            elsif cmd_init_i = '1' then
                etat_fut <= INIT_1;
            elsif to_target_i = '1' then
                etat_fut <= MOVE_TO_TARGET;
            elsif man_dir_0_i = '1' and  man_dir_1_i = '0' then
                etat_fut <= MAN_CLOCKWISE;
            elsif man_dir_1_i = '1' and  man_dir_0_i = '0' then
                etat_fut <= MAN_COUNTER_CLOCKWISE;
            else
                etat_fut <= etat_pres;
            end if;

        when START_CALIBRATION =>
            init_pos_o <= '1';
            busy_o <= '1';

            etat_fut <= CALIBRATING_1;

        when CALIBRATING_1 =>
            force_dir_1_o <= '1';
            force_low_speed_o <= '1';
            run_o <= '1';
            busy_o <= '1';

            if optic_i = '1' then
                etat_fut <= CALIBRATING_2;
            else
                etat_fut <= etat_pres;
            end if;

        when CALIBRATING_2 =>
            force_dir_1_o <= '1';
            force_low_speed_o <= '1';
            run_o <= '1';
            busy_o <= '1';

            if idx_i = '1' then
                etat_fut <= SET_IDX_POS;
            else
                etat_fut <= etat_pres;
            end if;

        when SET_IDX_POS =>
            save_idx_o <= '1';
            busy_o <= '1';

            etat_fut <= SET_TARGET_INIT_POS;

        when SET_TARGET_INIT_POS =>
            init_target_o <= '1';

            etat_fut <= BACK_TO_HOME;

        when INIT_1 =>
            force_dir_1_o <= '1';
            force_low_speed_o <= '1';
            run_o <= '1';
            busy_o <= '1';

            if optic_i = '1' then
                etat_fut <= INIT_2;
            else
                etat_fut <= etat_pres;
            end if;

        when INIT_2 =>
            force_dir_1_o <= '1';
            force_low_speed_o <= '1';
            run_o <= '1';
            busy_o <= '1';

            if idx_i = '1' then
                etat_fut <= GET_IDX_POS;
            else
                etat_fut <= etat_pres;
            end if;

        when GET_IDX_POS =>
            retrieve_idx_o <= '1';
            busy_o <= '1';

            etat_fut <= SET_TARGET_INIT_POS;

        when MOVE_TO_TARGET =>
            run_o <= '1';
            busy_o <= '1';

            if cmd_cal_i = '1' then
                etat_fut <= START_CALIBRATION;
            elsif cmd_init_i = '1' then
                etat_fut <= INIT_1;
            elsif target_reached_i = '1' then
                etat_fut <= START;
            else
                etat_fut <= etat_pres;
            end if;

        when BACK_TO_HOME =>
            force_dir_0_o <= '1';
            run_o <= '1';
            busy_o <= '1';

            if target_reached_i = '1' then
                etat_fut <= START;
            else
                etat_fut <= etat_pres;
            end if;

        when MAN_CLOCKWISE =>
            force_dir_0_o <= '1';
            run_o <= '1';
            busy_o <= '1';

            if cmd_cal_i = '1' then
                etat_fut <= START_CALIBRATION;
            elsif cmd_init_i = '1' then
                etat_fut <= INIT_1;
            elsif (man_dir_0_i = '0') then
                etat_fut <= START;
            else
                etat_fut <= etat_pres;
            end if;

        when MAN_COUNTER_CLOCKWISE =>
            force_dir_1_o <= '1';
            run_o <= '1';
            busy_o <= '1';

            if cmd_cal_i = '1' then
                etat_fut <= START_CALIBRATION;
            elsif cmd_init_i = '1' then
                etat_fut <= INIT_1;
            elsif (man_dir_1_i = '0') then
                etat_fut <= START;
            else
                etat_fut <= etat_pres;
            end if;

        when others =>
            etat_fut <= START;
        end case;
    end process;

    Mem:process (clk_i, reset_i)
    begin
        if reset_i = '1' then
            etat_pres <= START;
        elsif rising_edge(clk_i) then
            etat_pres <= etat_fut;
        end if;
    end process;

end m_etat;