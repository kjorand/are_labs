-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : top_gen.vhd
--
-- Description  : Generateur du signal top
--
-- Auteur       : Flavio Capitao
-- Date         : 12.01.2017
-- Version      : 1.0
--
--
--| Modifications |-------------------------------------------------------------
--  Ver.    Auteur                  Date        Commentaires
--  1.0     Leandro SARAVAIA MAIA   15.06.2023  Première itération CSN
--  2.0     Rachel  LEUPI           25.01.2024  adaptation clk 50 Mhz ARE
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

entity top_gen is
    port (
        reset_i      : in  std_logic;
        -- On part du principe que la clock fournit cadencé à 50MHz !!
        clk_i        : in  std_logic;
        speed_i      : in  std_logic_vector(1 downto 0);
        top_o        : out std_logic
    );
end top_gen;

architecture behav of top_gen is
    -- bits requis pour décompter avec 100ms
    constant NB_BITS_DECOMPTEUR: integer := 24;

    constant DECOMPTEUR_VIT_00 : integer := 5000000;  -- 100 ms
    constant DECOMPTEUR_VIT_01 : integer := 2000000;  -- 40 ms
    constant DECOMPTEUR_VIT_10 : integer := 1000000;  -- 20 ms
    constant DECOMPTEUR_VIT_11 : integer := 500000;   -- 10 ms

    signal cpt_pres_s, cpt_fut_s : unsigned(NB_BITS_DECOMPTEUR - 1 downto 0);
begin

    cpt_fut_s <= cpt_pres_s - 1 when cpt_pres_s /= 0 else
                 -- Recharge le decompteur
                 To_Unsigned(DECOMPTEUR_VIT_00 - 1, NB_BITS_DECOMPTEUR) when speed_i = "00" else
                 To_Unsigned(DECOMPTEUR_VIT_01 - 1, NB_BITS_DECOMPTEUR) when speed_i = "01" else
                 To_Unsigned(DECOMPTEUR_VIT_10 - 1, NB_BITS_DECOMPTEUR) when speed_i = "10" else
                 To_Unsigned(DECOMPTEUR_VIT_11 - 1, NB_BITS_DECOMPTEUR) when speed_i = "11" else
					  (others => 'X'); -- simulation
    process (clk_i, reset_i)
    begin
        if reset_i = '1' then
            cpt_pres_s <= (others => '0');
        elsif rising_edge(clk_i) then
            cpt_pres_s <= cpt_fut_s;
        end if;
    end process;

    -- Generation du pulse
    top_o <= '1' when cpt_pres_s = 0 else '0';

end behav;