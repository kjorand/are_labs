------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
--
-- File                 : gestion_IO.vhd
-- Author               :
-- Date                 : 07.11.2023
--
-- Context              : Avalon user interface
--
------------------------------------------------------------------------------------------
-- Description :
--
------------------------------------------------------------------------------------------
-- Dependencies :
--
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer    Comments
-- 0.0    See header              Initial version

------------------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity gestion_IO is
  port(
    clk_i               : in  std_logic;
    reset_i             : in  std_logic;

    button_i            : in std_logic_vector(3 downto 0);
    switch_i            : in std_logic_vector(9 downto 0);
    led_i               : in std_logic_vector(7 downto 0);
    hex_low_i           : in std_logic_vector(27 downto 0);
    hex_high_i          : in std_logic_vector(13 downto 0);

    cs_led_wr_i         : in std_logic;
    cs_hex_wr_high_i    : in std_logic;
    cs_hex_wr_low_i     : in std_logic;

    button_sync_o       : out std_logic_vector(3 downto 0);
    switch_sync_o       : out std_logic_vector(9 downto 0);
    led_o               : out std_logic_vector(7 downto 0);
    hex0_o              : out std_logic_vector(6 downto 0);
    hex1_o              : out std_logic_vector(6 downto 0);
    hex2_o              : out std_logic_vector(6 downto 0);
    hex3_o              : out std_logic_vector(6 downto 0);
    hex4_o              : out std_logic_vector(6 downto 0);
    hex5_o              : out std_logic_vector(6 downto 0)

  );
end gestion_IO;

architecture rtl of gestion_IO is
    constant ones_button_sized : std_logic_vector(button_i'range) := (others => '1');

begin
    process (clk_i, reset_i)
    begin
        if reset_i = '1' then
            led_o <= (others => '0');
            switch_sync_o <= switch_i;
            button_sync_o <= button_i xor ones_button_sized; -- Transforming active-low to a more "Normal" (active-high)
            hex0_o <= (others => '0');
            hex1_o <= (others => '0');
            hex2_o <= (others => '0');
            hex3_o <= (others => '0');
            hex4_o <= (others => '0');
            hex5_o <= (others => '0');

        elsif rising_edge(clk_i) then
            button_sync_o <= button_i xor ones_button_sized; -- Transforming active-low to a more "Normal" (active-high)
            switch_sync_o <= switch_i;
            if cs_led_wr_i = '1' then
                led_o <= led_i;
            end if;

            if cs_hex_wr_low_i = '1' then
                hex0_o <= hex_low_i(6 downto 0);
                hex1_o <= hex_low_i(13 downto 7);
                hex2_o <= hex_low_i(20 downto 14);
                hex3_o <= hex_low_i(27 downto 21);
            end if;

            if cs_hex_wr_high_i = '1' then
                hex4_o <= hex_high_i(6 downto 0);
                hex5_o <= hex_high_i(13 downto 7);
            end if;

        end if;
    end process;

end rtl;