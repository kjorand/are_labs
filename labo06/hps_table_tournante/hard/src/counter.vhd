------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
--
-- File                 : counter.vhd
-- Author               : Rachel Leupi / Kévin Jorand
-- Date                 : 23.01.2023
--
-- Context              : Avalon user interface
--
------------------------------------------------------------------------------------------
-- Description :
--
------------------------------------------------------------------------------------------
-- Dependencies : none
--
------------------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity counter is
  port(
    clk_i               : in  std_logic;
    reset_i             : in  std_logic;
    inc_i               : in  std_logic;
    dec_i               : in  std_logic;
    load_i              : in  std_logic;
    value_i             : in  std_logic_vector(15 downto 0);

    counter_o           : out std_logic_vector(15 downto 0)
  );
end counter;

architecture rtl of counter is

-- Signaux
signal counter_value_s : unsigned(15 downto 0) := x"7530";


begin
    process (clk_i, reset_i)
    begin
        if reset_i = '1' then
            counter_value_s <= x"7530";

        elsif rising_edge(clk_i) then

            if load_i = '1' then
                counter_value_s <= unsigned(value_i);

            else

                if inc_i = '1' then
                    counter_value_s <= counter_value_s + 1;
                elsif dec_i = '1' then
                    counter_value_s <= counter_value_s - 1;
                end if;

            end if;

        end if;
    end process;

    counter_o <= std_logic_vector(counter_value_s);

end rtl;