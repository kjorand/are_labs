# Laboratoire 6 - Commande d’une table tournante
Auteurs : Kévin Jorand, Rachel Leupi

## Objectifs

L'objectif est de piloter la table tournante selon les spécifications fournies. En particulier, on souhaites pouvoir traiter diverses séquences, et commandes. On souhaites aussi pouvoir gérer des fin de courses virtuelles.

# Conception VHDL

## Plan d'adressage

| Adr AVL | Adr CPU       | Read                                                                                                                                                            | Write                                                                                                                                                                    | Description        |
| ------- | ------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------ |
| 0x00    | 0x00          | [31..0] Constante ID interface                                                                                                                                  | not used                                                                                                                                                                 | Interface ID       |
| 0x01    | 0x04          | [31..4] "0..0" <br/> [3..0] buttons                                                                                                                             | not used                                                                                                                                                                 | buttons            |
| 0x02    | 0x08          | [31..10] "0..0" <br/> [9..0] switchs                                                                                                                            | not used                                                                                                                                                                 | switchs            |
| 0x03    | 0x0C          | [31..8] "0..0" <br/> [7..0] leds                                                                                                                                | [31..8] reserved <br/> [7..0] leds                                                                                                                                       | leds               |
| 0x04    | 0x10          | [31..28] "0..0" <br/> [27..21] hex3 <br/>  [20..14] hex2 <br/>  [13..7] hex1 <br/>  [6..0] hex0                                                                 | [31] reserved ; [30..24] hex3 <br/> [23] reserved ; [22..16] hex2 <br/> [15] reserved ; [14..8] hex1 <br/> [7] reserved ; [6..0] hex0                                    | hex3_0             |
| 0x05    | 0x14          | [31..14] "0..0" <br/> [13..7] hex5 <br/>  [6..0] hex4                                                                                                           | [31..16] reserved <br/> [15] reserved ; [14..8] hex5 <br/> [7] reserved ; [6..0] hex4                                                                                    | hex5_4             |
| 0x06    | 0x18          | [31..2] "0..0" <br/> [1..0] speed                                                                                                                               | [31..2] reserved <br/> [1..0] speed                                                                                                                                      | speed              |
| 0x07    | 0x1C          | [31] limit_high <br/> [30] limit_low <br/> [29..6] "0..0" <br/> [5] man_dir_1 <br/> [4] man_dir_0 <br/> [3] interrupt_enable <br/> [2] busy <br/> [1..0] "0..0" | [31..30] ack_interrupt <br/> [29..6] reserved <br/> [5] man_dir_1 <br/> [4] man_dir_0 <br/> [3] interrupt_enable <br/> [2] reserved <br/> [1] seq_init <br/> [0] seq_cal | commands and state |
| 0x08    | 0x20          | [31..16] "0..0" <br/> [15..0] target                                                                                                                            | [31..16] reserved <br/> [15..0] target                                                                                                                                   | target             |
| 0x09    | 0x24          | [31..16] "0..0" <br/> [15..0] position                                                                                                                          | [31..0] reserved                                                                                                                                                         | position           |
| 0x0A    | 0x28          | [31..16] "0..0" <br/> [15..0] idx                                                                                                                               | [31..0] reserved                                                                                                                                                         | idx                |
| 0x0B    | 0x2C ... 0xFF | available for news functionality                                                                                                                                | available for news functionality                                                                                                                                         | description        |
## Schéma globale du système
![image](img/schema.jpeg)

## Gestion IO
Ce module permet la synchronisation des entrées et sorties en provenance de la DE1_SoC tel que les leds, les switchs, les bouton ou encore les 7segments.
Il est composé de simple flip-flop pour synchroniser chaque entrée / sorties.

## Accès en lecture

L’accès en lecture se fait comme une composition (virtuelle) d'un décodeur d'adresse et d'un multiplexeur. Celui-ci prend tous les signaux accessibles en lecture et renvoie sur le bus avalon le signal approprié en fonction de l'adresse.

Voici un résumé des équations auxquelles il répond :
```vhd
case avl_address_i(7 downto 0) is
    when x"00" => -- CPU addr : 00
        avl_readdata_o              <= AVALON_ID;
    when x"01" => -- CPU addr : 04
        avl_readdata_o(3 downto 0)  <= button_s;
    when x"02" => -- CPU addr : 08
        avl_readdata_o(9 downto 0)  <= switch_s;
    when x"03" => -- CPU addr : 0C
        avl_readdata_o(7 downto 0)  <= led_s;
    when x"04" => -- CPU addr : 10
        avl_readdata_o(27 downto 0) <= hex3_s & hex2_s & hex1_s & hex0_s;
    when x"05" => -- CPU addr : 14
        avl_readdata_o(13 downto 0) <= hex5_s & hex4_s;
    when x"06" => -- CPU addr : 18
        avl_readdata_o(1 downto 0)  <= speed_top_gen_s;
    when x"07" => -- CPU addr : 1C
        avl_readdata_o <= limite_high_s & limite_low_s
                                        & "000000000000000000000000"
                                        & man_dir_1_s & man_dir_0_s
                                        & irq_en_s & busy_s & "00";
    when x"08" => -- CPU addr : 20
        avl_readdata_o(15 downto 0) <= target_saved_s;
    when x"09" => -- CPU addr : 0x24
        avl_readdata_o(15 downto 0) <= position_s;
    when x"0A" => -- CPU addr : 0x28
        avl_readdata_o(15 downto 0) <= idx_pos_s;
    when others =>
        avl_readdata_o              <= UNUSED;
end case;
```
cf : schéma globale du système

## Accès en écriture

L’accès en écriture quand à lui fonctionne avec un décodeur d'adresse (auquel on ajouterait un démultiplexeur). Selon l'adresse, un chip select va s'activer pour mettre à jour un le registre correspondant avec les données provenant du bus avalon.

Le démultiplexeur réponds au équations suivante :

```vhd
case avl_address_i(7 downto 0) is
    when x"03" => -- CPU addr : 0C
        cs_led_wr_s   <= '1';

    when x"04" => -- CPU addr : 10
        cs_hex_wr_low_s   <= '1';

    when x"05" => -- CPU addr : 14
        cs_hex_wr_high_s  <= '1';

    when x"06" => -- CPU addr : 18
        cs_speed_s        <= '1';

    when x"07" => -- CPU addr : 1C
        cs_cmd_state_s <= '1';

    when x"08" => -- CPU addr : 20
        cs_target_wr_s <= '1';

    when others =>
        -- for simulation
```

Voici une courte description des actions des chip select :

- cs_led_wr_s       : Écriture sur le registre des leds
- cs_hex_low_s      : Écriture sur le registre des 7 segments [hex3 - hex0]
- cs_hex_wr_high_s  : Écriture sur le registre des 7 segments [hex5 - hex4]
- cs_speed_s        : Écriture sur le registre gérant la vitesse de generation de tops
- cs_cmd_state_s    : Écriture sur le registre qui gère les point suivant
[31..30] ack_interrupt, [29..6] reserved,[5] man_dir_1,[4] man_dir_0,[3] interrupt_enable,[2] reserved,[1] seq_init
[0] seq_cal
- cs_target_wr_s    : Écriture sur la position souhaité.

cf : schéma global du système

## Générateur de TOP

### Conception
Pour ce module on a pu récupérer un module codé lors du laboratoire cd la table tournante en CSN. Cela expliquerait de potentielle ressemblance avec le laboratoire de Leandro. Voici son fonctionnement :

Voici comment le module fonctionne :

Un compteur est initialisé selon la valeur de la vitesse :

- Vitesse 00 : La période souhaitée est de 100ms ainsi tout les 5000000 cycles d'horloge une pulse est envoyée.
- Vitesse 01 : La période souhaitée est de 40ms ainsi tout les 2000000 cycles d'horloge une pulse est envoyée.
- Vitesse 10 : La période souhaitée est de 20ms ainsi tout les 1000000 cycles d'horloge une pulse est envoyée.
- Vitesse 11 : La période souhaitée est de 10ms ainsi tout les 500000 cycles d'horloge une pulse est envoyée.

Le signal de sortie est activé ('1') lorsque le compteur présent atteint 0, indiquant ainsi la fin de la période de décomptage.
Le signal de sortie est désactivé ('0') pendant le reste du temps.

En résumé, ce module génère donc un signal de sortie périodique en fonction de la vitesse donnée en entrée qui initialise la période de décomptage en conséquence. Ce signal de sortie est utilisé pour générer des impulsions périodiques à la table tournante.

## FSM

Tout les signaux qui ne sont pas explicitement à '1' dans un état sont à '0'

![FSM](./img/FSM.png)

Voici une description des signaux d'entrés et de sorties de la machine d'état. Ceci explicite très bien les actions que génère la MSS.

### Inputs
- `cmd_cal_i` => commande de calibration depuis avalon
- `cmd_init_i` => commande d'initialisation depuis avalon
- `to_target_i` => Commande pour aller à la position souhaitée : il est généré en écrivant dans `target_register` depuis avalon
- `optic_i` => signal depuis le capteur optique
- `idx_i` => signal depuis le capteur d'index
- `target_reach_i` => signal correspondant à `state_register` == `target_register`
- `man_dir_1_i` => état du bit `man_dir_1` dans`command and state register`
- `man_dir_0_i` => état du bit `man_dir_0` dans `command and state register`

### Outputs
- `init_pos` => set `state_register` à 30_000, la position initiale
- `init_target` => set `target_register` à 30_000, la position initiale
- `force_low_speed` => force la vitesse lente sur la table
- `force_dir_1` => force la direction de la table à 1
- `force_dir_0` => force la direction de la table à 0
- `run` => enable la table
- `save_idx` => copie `state_register` sur `idx_register`
- `retrieve_idx` => copie `idx_register` sur `state_register`
- `busy` => flag : la table est en train de faire une sequence => possible de le lire sur `command and state register`

### `state_register`
- incrémenté / décrémenté depuis l'entré de la table
- continuellement comparé au limite min/max
- flag de fin de course si limite dépassé
- si les interruption sont active : lève une IRQ

### `irq`
- levé: voir au dessus
- baissé à l'écriture de `11` dans le `ack_interrupt`.

### direction
Quand pas forcé, dir = (`target_register` > `state_register`);

# Test VHDL

## Testé via VSIM :

- la lecture de la Cst du bus Avalon s'effectue correctement à l'adresse CPU 0x00
- La lecture des boutons s'effectue correctement à l'adresse CPU 0x04
- la lecture des switchs s'effectue correctement à l'adresse CPU 0x08
- l'écriture des leds s'effectue correctement à l'adresse CPU 0x0C  ainsi que la lecture
- l'écriture des 7 segments de 3 à 0  s'effectue correctement à l'adresse CPU 0x10 ainsi que la lecture
- l'écriture des 7 segments de 5 et 4 s'effectue correctement à l'adresse CPU 0x14 ainsi que la lecture
- la lecture et l'écriture de la vitesse à l'adresse CPU fonctionne correctement
- Les différentes transition de la machine d'état on aussi été testé dans vSim

## Testé via les leds

On a redirigé certain signaux sur le port des leds pour pouvoir mesurer si ceux-ci étaient bien lancés quand on le souhaitait. Pour la majorité, la simple vu des leds respectives allumées était suffisant pour constater le bon fonctionnement de ces signaux. Pour d'autres, l'utilisation de l'oscilloscope s'est avérée nécessaire.

Grâce a ce procédé les différents signaux de sortie de la fsm ont été testé et tous fonctionnent correctement ainsi que les different signaux se situant dans cmd_state register(limite_high/low, man_dir_0/1, irq_en busy) et les limites de zones.

La génération de top à été contrôlé à l'oscilloscope. Voici nos résultat :
![image](img/top10ms.jpg)
on obtient bien 10 ms de période à la mesure du toggle de la led à vitesse 11.
![image](img/top20ms.jpg)
on obtient bien 20 ms de période à la mesure du toggle de la led à vitesse 10.
![image](img/top40ms.jpg)
on obtient bien 40 ms de période à la mesure du toggle de la led à vitesse 01.
![image](img/top100ms.jpg)
on obtient bien 100 ms de période à la mesure du toggle de la led à vitesse 00.

# Tests C

Nous avons écrit le C de manière "incrémentale" de manière à pouvoir tester les fonctionnalités au fur et à mesure. Nous nous sommes rendus compte que certains choix d'implémentation pouvaient peut-être laisser à désirer (autant en C qu'en VHDL !) ... Mais après un certain nombre d'heures nous avons renoncé à reprendre complètement l'implémentation de choses qui étaient déjà fonctionnelles et testées fonctionnelles.

Nous avons typiquement écrit une première version du C qui testait simplement l'arrivée de l'interruption. Mais en l'absence d'une version pleinement fonctionnelle du VHDL, nous avons pris le parti d'implémenter tout de même la logique suivante (il serait facile de pouvoir aussi changer pour utiliser la même logique avec une scrutation des flags, si ces derniers étaient correctement placés).

C'est aussi à ce moment que nous nous sommes rendus compte de certains "mauvais choix" en C (mais à nouveaux, avons choisis de les contourner plutôt que de reprendre à zéro).

## Problèmes rencontrés

1-2 exemples de problèmes rencontrés:

- Nous pensions pouvoir simplement faire les déplacements manuels en donnant faisant un target à la FPGA ... ce qui aurait eu pour avantage de factoriser le code autant que possible... Mais nous sommes rendu compte que cela n'était pas possible et avons ainsi ajouté des états à notre MSS toujours en essayant de factoriser et réutiliser ce que nous avions déjà.

- Toujours avec le mode manuel, nous avions au départ trop d'écritures (probablement ?) ce qui conduisait à un comportement "ralenti" : la table semblait "manquer" certains tops. C'est la raison qui nous a poussé à avoir ces flags dans le C [qui n'est pas tout à fait idéal puisqu'on double l'état et on introduit un risque de désynchronisation]. Cela a toutefois résolu le problème courant.

- Les fin de course ne sont pas fonctionnelles. le problème vient assurément du VHDL mais le manque de temps nous empêche de continuer le debugging. Les lignes de code ont été laissées dans les fichiers mais rendues inefficaces pour la présentation de lundi. La sortie `avl_irq_o` a été hardcodé à '0' pour le vhdl tandis que les interrupts jamais enable pour le c.

## Conclusion

En conclusion, nous avons retenu certaines leçon quand à la conception, comme par exemple de pensez à tous les uses-case dès le début et non pas de faire les choses incérmentalement (dans le désordre). De plus nous repartons avec l'apprentissage de nos erreurs. Il est presque frustrant de voir à la fin ce qui "aurait dû être fait" ... mais de manquer de ressource pour le faire juste et bien ... devant ainsi laisser ce qui est brouillon, mauvais, non-fonctionnel voir faux ...

### Note

Absence maladie : Rachel Leupi (1 semaine)
