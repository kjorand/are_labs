/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : hps_application.c
 * Author               : 
 * Date                 : 
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: Turn ON leds and 7 segment display depending the value of button and switch, for DE1-SoC board
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 
 *
*****************************************************************************************/
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include "axi_lw.h"
#include "pio_function.h"

#define KEY0 0x1
#define KEY1 0x2
#define KEY2 0x4
#define KEY3 0x8

int __auto_semihosting;


void key0(){
    uint32_t swVal = Switchs_read(); // Only read once to avoid incoherent data
    switch (swVal >> 8){ // Above are set to 0 by Switchs_read
        case 0x0:
            Leds_write(swVal & 0xFF);
            break;
        case 0x1:
            Leds_set(swVal & 0xFF);
            break;
        case 0x2:
            Leds_clear(swVal & 0xFF);
            break;
        case 0x3:
            Leds_toggle(0xFF);
            break;
    }
}

void key1(){
    uint32_t swVal = Switchs_read(); // Only read once to avoid incoherent data
    Seg7_write_hex(0, swVal & 0xF);
    Seg7_write_hex(1, (swVal >> 4) & 0xF);
}

void key2(){
    static uint8_t seg = 0x01;
    const uint8_t SEG_MAX = 0x20; 
    seg <<= 1;
    if(seg > SEG_MAX){
        seg = 0x01;
    }
    Seg7_write(2, seg);
}

void key3(){
    static uint8_t valHex3 = 0;
    valHex3 += 1;
    Seg7_write_hex(3, (uint32_t)valHex3&0xF); // No need to test and branch and 0 the var!
}


int main(void){
    
    printf("Laboratoire: Programme simple avec E/S du HPS \n");

    printf("ID : 0x%"PRIX32" \n", *(volatile uint32_t *)(AXI_LW_HPS_FPGA_BASE_ADD + PIO_ID));

    Switchs_init();
    Leds_init();
    Keys_init();
    Segs7_init();
    
    uint8_t pressed;
    while(1){ //? Use for loop and key(int) function instead of multiple keyX() ?
        pressed = Key_pressed();
        if(pressed & KEY0){
            key0();
        }
        if(pressed & KEY1){
            key1();
        }
        if(pressed & KEY2){
            key2();
        }
        if(pressed & KEY3){
            key3();
        }
    }

}
