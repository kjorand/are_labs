/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : timer.c
 * Author               : 
 * Date                 : 
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: Pio function
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 
 *
*****************************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include "timer.h"

#define CHECK_ERRORS
#ifdef CHECK_ERRORS
	#include <inttypes.h>
	#include <stdio.h>
#endif /* CHECK_ERRORS */

#define TIMER_VAL 2500000 /* 25M * 0.1S = 2.5M */


void Timer_set_value(uint32_t value){
    /* Loading the Timer Countdown Value
        When a timer counter is enabled after being reset or disabled, the count value is loaded from the timer1loadcount register; this occurs in both free-running and user-defined count modes. †
        When a timer counts down to 0, it loads one of two values, depending on the timer operating mode: †
            • User-defined count mode—timer loads the current value of the timer1loadcount register. Use this mode if you want a fixed, timed interrupt. Designate this mode by writing a 1 to the timer1_mode bit of the timer1controlreg register. †
            • Free-running mode—timer loads the maximum value (0xFFFFFFFF). The timer max count value allows for a maximum amount of time to reprogram or disable the timer before another interrupt occurs. Use this mode if you want a single timed interrupt. Enable this mode by writing a 0 to the timer1_mode bit of the timer1controlreg register. †
    */
    TIMER_REG(TIMER1LOADCOUNT_OFFSET) = value;
}

void Timer_set_100ms_value(){
    Timer_set_value(TIMER_VAL);
}

void Timer_init(){
    /* Initialization
        To initialize the timer, perform the following steps: †
        1. Initialize the timer through the timer1controlreg register: †
            • Disable the timer by writing a 0 to the timer1 enable bit (timer1_enable) of the timer1controlreg register. †
            Note: Before writing to a timer1 load count register (timer1loadcount), you must disable the timer by writing a 0 to the timer1_enable bit of the timer1controlreg register to avoid potential synchronization problems. †
            • Program the timer mode—user-defined count or free-running—by writing a 0 or 1, respectively, to the timer1 mode bit (timer1_mode) of the timer1controlreg register. †
            • Set the interrupt mask as either masked or not masked by writing a 1 or 0, respectively, to the timer1_interrupt_mask bit of the timer1controlreg register. †
        2. Load the timer counter value into the timer1loadcount register. †
        3. Enable the timer by writing a 1 to the timer1_enable bit of the timer1controlreg register. †
    */
    // Disable Timer
    Timer_disable();

    // Programm Timer
    TIMER_REG(TIMER1CONTROLREG_OFFSET) |= TIMER_MODE_BIT;
    TIMER_REG(TIMER1CONTROLREG_OFFSET) &= ~TIMER_INTERRUPT_MASK_BIT;

    // Load value
    Timer_set_100ms_value();

    // Enable timer
    // => not done in init
}

void Timer_enable(){
    /* Enabling the Timer
        When a timer transitions to the enabled state, the current value of timer1loadcount register is loaded into the timer counter. †
            1. To enable the timer, write a 1 to the timer1_enable bit of the timer1controlreg register
    */
    TIMER_REG(TIMER1CONTROLREG_OFFSET) |= TIMER_ENABLE_BIT;
}

void Timer_disable(){
    /* Disabling the Timer
        When the timer enable bit is cleared to 0, the timer counter and any associated registers in the timer clock domain, are asynchronously reset. †
            1. To disable the timer, write a 0 to the timer1_enable bit. †
    */
    TIMER_REG(TIMER1CONTROLREG_OFFSET) &= ~TIMER_ENABLE_BIT;
}

uint32_t Timer_get_running_state(){
    return TIMER_REG(TIMER1CONTROLREG_OFFSET) & TIMER_ENABLE_BIT;
}

void Timer_clear_interrupt(){
    /* Clearing the Interrupt
        An active timer interrupt can be cleared in two ways.
            1. If you clear the interrupt at the same time as the timer reaches 0, the interrupt remains asserted. This action happens because setting the timer interrupt takes precedence over clearing the interrupt. †
            2. To clear an active timer interrupt, read the timer1eoi register or disable the timer. When the timer is enabled, its interrupt remains asserted until it is cleared by reading the timer1eoi register. †
        Checking the Interrupt Status
            You can query the interrupt status of the timer without clearing its interrupt.
                1. To check the interrupt status, read the timer1intstat register. †
    */
    do{
        volatile uint32_t EOIR = TIMER_REG(TIMER1EOI_OFFSET); // Clears timer
    }while(TIMER_REG(TIMER1INTSTAT_OFFSET) & TIMER_INTSTAT_BIT);
}

