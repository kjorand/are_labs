#ifndef __TIMER_H__
#define __TIMER_H__

/* Module reset register */
#define MODULE_RESET_REG           0xFFD05014
#define OSC1TIM0_RESET_BIT         0x00000100

#define TIMER_REG(_x_)   *(volatile uint32_t *)(OSC1TIMER0_BASE_ADDRESS + _x_) /* _x_ is the offset from upper list */

/* Timer base address */
#define OSC1TIMER0_BASE_ADDRESS    0xFFD00000

/* Timer registers */
#define TIMER1LOADCOUNT_OFFSET     0x0  /* 32 RW 0x0        Timer1 Load Count Register */
#define TIMER1CURRENTVAL_OFFSET    0x4  /* 32 RO 0x0        Timer1 Current Value Register */
#define TIMER1CONTROLREG_OFFSET    0x8  /* 32 RW 0x0        Timer1 Control Register */
#define TIMER1EOI_OFFSET           0xC  /* 32 RO 0x0        Timer1 End-of-Interrupt Register */
#define TIMER1INTSTAT_OFFSET       0x10 /* 32 RO 0x0        Timer1 Interrupt Status Register */
#define TIMERSINTSTAT_OFFSET       0xA0 /* 32 RO 0x0        Timers Interrupt Status Register */
#define TIMERSEOI_OFFSET           0xA4 /* 32 RO 0x0        Timers End-of-Interrupt Register */
#define TIMERSRAWINTSTAT_OFFSET    0xA8 /* 32 RO 0x0        Timers Raw Interrupt Status Register */
#define TIMERSCOMPVERSION_OFFSET   0xAC /* 32 RO 0x3230352A Timers Component Version Register */

#define TIMER_REG(_x_)   *(volatile uint32_t *)(OSC1TIMER0_BASE_ADDRESS + _x_) /* _x_ is the offset from upper list */

/* BITS */
// TIMER1CONTROLREG
#define TIMER_MODE_BIT           0x2 /* timer1_mode Sets operating mode. Value Description : 0x0 Free-running mode / 0x1 User-defined count mode - RW 0x0 */
#define TIMER_ENABLE_BIT         0x1 /* timer1_enable Timer1 enable/disable bit. Value Description : 0x0 Timer1 Disabled / 0x1 Timer1 Enabled - RW 0x0 */
#define TIMER_INTERRUPT_MASK_BIT 0x4 /* timer1_interrupt_mask Timer1 interrupt mask Value Description : 0x0 interrupt not masked (enabled) / 0x1 interrupt masked (disabled) - RW 0x0 */

// TIMER1INTSTAT
#define TIMER_INTSTAT_BIT 0x1 /* 0 timer1intstat Provides the interrupt status for Timer1. The status reported is after the interrupt mask has been applied. Reading from this register does not clear any active interrupts. Value Description : 0x0 Timer1 interrupt is not active / 0x1 Timer1 interrupt is active - RO 0x0 */


// Initialises and sets up the timer
void Timer_init();

// Enables the timer (lets it run)
void Timer_enable();

// Disable the timer (stops it from running)
void Timer_disable();

// Returns 0 if the timer is disabled, > 0 (TIMER_ENABLE_BIT) otherwise
uint32_t Timer_get_running_state();

// Clears TIMER interrupt (not related to GIC interrupts which must be cleared independantly)
void Timer_clear_interrupt();

#endif /* __TIMER_H__ */
