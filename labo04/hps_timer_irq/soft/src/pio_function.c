/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : pio_function.c
 * Author               : 
 * Date                 : 
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: Pio function
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 
 *
*****************************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include "pio_function.h"
#include "hex_val.h"

#define CHECK_ERRORS
#ifdef CHECK_ERRORS
	#include <inttypes.h>
	#include <stdio.h>
#endif /* CHECK_ERRORS */


// TO BE COMPLETE

void Switchs_init(void){
    PIO0_REG(PIO_DIRECTION) &= ~SWITCHS_BITS; // Direction : input

    // Interrupts not treated yet !
}

void Leds_init(void){
    PIO0_REG(PIO_DIRECTION) |= LEDS_BITS; // Direction : output

    // Start value
    Leds_clear(0x3FF); // Switching all off
}

void Keys_init(void){
    PIO0_REG(PIO_DIRECTION) &= ~KEY_BITS; // Direction : input

    // Interrupts not treated yet !
}

void Segs7_init(void){
    PIO1_REG(PIO_DIRECTION) |= HEX0_BITS | HEX1_BITS | HEX2_BITS | HEX3_BITS; // Direction : output

    // Start value
    Seg7_write_hex(0, 0x0); // Affiche 0
    Seg7_write_hex(1, 0x0); // Affiche 0
    Seg7_write_hex(2, 0x0); // Affiche 0
    Seg7_write_hex(3, 0x0); // Affiche 0
}

uint32_t Switchs_read(void){
    return (PIO0_REG(PIO_DATA) & SWITCHS_BITS) >> SWITCHS_BITS_OFFSET; // Returns directly the value, at position 0
}

void Leds_write(uint32_t value){
    Leds_write_with_mask(value, LEDS_BITS >> LEDS_BITS_OFFSET);
}

void Leds_write_with_mask(uint32_t value, uint32_t mask){
    Leds_set(value & mask); // set required bits
    Leds_clear( (~value) & mask);  // clear required bits
}

void Leds_set(uint32_t maskleds){
    PIO0_REG(PIO_OUTSET) = (maskleds << LEDS_BITS_OFFSET) & LEDS_BITS;
}

void Leds_clear(uint32_t maskleds){
    PIO0_REG(PIO_OUTCLEAR) = (maskleds << LEDS_BITS_OFFSET) & LEDS_BITS;
}

void Leds_toggle(uint32_t maskleds){
    uint32_t curLeds = (PIO0_REG(PIO_DATA) & LEDS_BITS)  >> LEDS_BITS_OFFSET;
    // Leds_write(( (PIO0_REG(PIO_DATA) & LEDS_BITS)  >> LEDS_BITS_OFFSET ) ^ maskleds); // Could be done in 2 parts : clear / set to NOT touch the other ones
    Leds_set(maskleds & (~curLeds));
    Leds_clear(maskleds & curLeds);
}

bool Key_read(int key_number){
#ifdef CHECK_ERRORS
    // Sanity checks / guard clauses
    if(key_number > MAX_KEY || key_number < 0){
        printf("Error in key choice (%d does not satisfie 0 <= X <= %d)\n", key_number, MAX_KEY);
        return false;
    }
#endif /* CHECK_ERRORS */

    // actual data writing
    return (bool)( (~PIO0_REG(PIO_DATA) >> (KEY_BITS_OFFSET + (unsigned)key_number)) & (uint32_t)0x1 );
}

uint8_t Key_pressed() {
    static uint8_t lastValue = 0xF;
    uint8_t currentValue = (PIO0_REG(PIO_DATA) & KEY_BITS) >> KEY_BITS_OFFSET;
    uint8_t pressed = (lastValue ^ currentValue) & lastValue; // forces last = 1 and current  = 0 (inversed logic)

    lastValue = currentValue;
    return pressed;
}

void Seg7_write(int seg7_number, uint32_t value){
#ifdef CHECK_ERRORS
    // Sanity checks / guard clauses
    if(seg7_number > MAX_HEX || seg7_number < 0){
        printf("Error in 7 seg choice (%d does not satisfie 0 <= X <= %d)\n", seg7_number, MAX_HEX);
        return;
    }
    if(value > HEX0_BITS){
        printf("Invalid 7 seg value (0x%"PRIX32" > 0xFF)\n", value);
        return;
    }
#endif /* CHECK_ERRORS */

    // actual data writing
    uint32_t out = (~value) & HEX0_BITS; // Active low!
    unsigned offset = (HEX_BITS_OFFSET * seg7_number);
    PIO1_REG(PIO_OUTSET) = out << offset;
    PIO1_REG(PIO_OUTCLEAR) = ( (~out) & HEX0_BITS) << offset;
}

void Seg7_write_hex(int seg7_number, uint32_t value){
#ifdef CHECK_ERRORS
    // Sanity checks / guard clauses
    if(value > 0xF){
        printf("Invalid 7 seg value (0x%"PRIX32" > 0xF)\n", value);
        return;
    }
#endif /* CHECK_ERRORS */

    // actual data writing
    Seg7_write(seg7_number, (uint32_t)VAL2SEG[value]);
}
