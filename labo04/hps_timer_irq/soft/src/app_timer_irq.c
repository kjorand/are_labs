/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : app_timer_irq.c
 * Author               : Anthony Convers
 * Date                 : 27.10.2022
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: Using timer IRQ on DE1-SoC board
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 0.0    27.10.2022  ACS           Initial version.
 *
*****************************************************************************************/
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include "axi_lw.h"
#include "pio_function.h"
#include "timer.h"
#include "interrupts.h"

#define KEY0 0x1
#define KEY1 0x2
#define KEY2 0x4
#define KEY3 0x8

#define LED8    0x100
#define LED9    0x200
#define LEDS7_0 0x0FF

#define NBR_7SEG 4

int __auto_semihosting;

static int timerValue;

void hps_timer_ISR(void){ // Used in exceptions.c
    Leds_toggle(LED9);
    Timer_clear_interrupt();
    timerValue -= 1;
    if(timerValue <= 0){
        Timer_disable();
    }
}

void set7Segs(uint32_t val){
    for(int i = 0; i < NBR_7SEG; ++i){
        Seg7_write_hex(i, val%10);
        val /= 10;
    }
}

void setCounter(uint32_t val){
    timerValue = val*10; // in 0.1s
    set7Segs(timerValue);
}


void key0(){
    /* Démarrage du minuteur */

    /* Start timer if some time is left */
    if(timerValue){
        Leds_set(LED8);
        Timer_enable();
    }
}

void key1(){
    /* Arrêt du minuteur */

    /* Stops timer + clears the led */
    Leds_clear(LED8);
    Timer_disable();
}

void key2(uint32_t swVal){
    /* Chargement val switchs sur leds et minuteur */
    Leds_write_with_mask(swVal, LEDS7_0); // To avoid overwriting the begining
    setCounter(swVal & LEDS7_0);

    if(Timer_get_running_state()){
        // Resets timer state
        Timer_disable();
        Timer_enable();
        Leds_clear(LED9); // Resets led9 state as well 
    }
}

int main(void){
    
    printf("Laboratoire: Timer IRQ \n");

    printf("ID : 0x%"PRIX32" \n", AXI_LW_REG(AXI_ID));

    Interrupt_init();

    Switchs_init();
    Leds_init();
    Keys_init();
    Segs7_init();

    Timer_init();
    
    uint8_t pressed;
    uint32_t swVal;
    while(1){
        swVal = Switchs_read(); // Only read once to avoid incoherent data
        pressed = Key_pressed();
        if(pressed & KEY0){
            key0();
        }
        if(pressed & KEY1){
            key1();
        }
        if(pressed & KEY2){
            key2(swVal);
        }

        // Deffered actions
        if(timerValue <= 0){
            Leds_clear(LED8);
            timerValue = 0;
        }
        set7Segs(timerValue);

    }

}
