/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : hex_val.h
 * Author               : Kévin Jorand
 * Date                 : 02.10.2023
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: Header file for hex to 7 seg conversion
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 0.0    02.10.2023  KJD          Initial version.
 *
*****************************************************************************************/

#ifndef __HEX_VAL_H__
#define __HEX_VAL_H__

#include <stdint.h>

static const uint8_t VAL2SEG[0x10] = {
    0x3F, // 0
    0x06, // 1
    0x5B, // 2
    0x4F, // 3
    0x66, // 4
    0x6D, // 5
    0x7D, // 6
    0x07, // 7
    0x7F, // 8
    0x6F, // 9
    0x77, // A
    0x7C, // B
    0x39, // C
    0x5E, // D
    0x79, // E
    0x71  // F
};

#endif /* __HEX_VAL_H__ */

