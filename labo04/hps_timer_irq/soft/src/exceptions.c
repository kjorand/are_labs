/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : execptions.c
 * Author               : Anthony Convers
 * Date                 : 27.10.2022
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: defines exception vectors for the A9 processor
 *        provides code that sets the IRQ mode stack, and that dis/enables interrupts
 *        provides code that initializes the generic interrupt controller
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Engineer      Comments
 * 0.0    27.10.2022  ACS           Initial version.
 *
*****************************************************************************************/
#include <stdint.h>

#include "address_map_arm.h"
#include "int_defines.h"
#include "interrupts.h"

#define DEBUG
#ifdef DEBUG
#include <stdio.h>
#endif

/* This file:
 * 1. defines exception vectors for the A9 processor
 * 2. provides code that sets the IRQ mode stack, and that dis/enables interrupts
 * 3. provides code that initializes the generic interrupt controller
*/
void hps_timer_ISR(void);

// Define the IRQ exception handler
void __attribute__ ((interrupt)) __cs3_isr_irq (void){
	/***********
	 * TO DO
	 **********/

	// Read the ICCIAR from the CPU Interface in the GIC  to determine which peripheral has caused an interrupt
	int interrupt_ID = REG(ICCIAR) & IAR_BITS;

	// Handle the interrupt if it comes from the timer
	if (interrupt_ID == TIMER_INT_NO){ // check if interrupt is from the KEYs
		hps_timer_ISR();
	}else{
		#ifdef DEBUG
			printf("INTERRUPT TRAP : %d\n", interrupt_ID);
		#endif
		while (1); // if unexpected, then stay here (to provide an easy debug)
	}

	// Write to the End of Interrupt Register (ICCEOIR) to 	clear interrupt from the CPU Interface
	REG(ICCEOIR) = interrupt_ID & EOIR_BITS;
    
	return;
} 

// Define the remaining exception handlers
void __attribute__ ((interrupt)) __cs3_reset (void)
{
	#ifdef DEBUG
		printf("INTERRUPT RESET MODE\n");
	#endif

    while(1);
}

void __attribute__ ((interrupt)) __cs3_isr_undef (void)
{
	#ifdef DEBUG
		printf("INTERRUPT UNDEF MODE\n");
	#endif
    while(1);
}

void __attribute__ ((interrupt)) __cs3_isr_swi (void)
{
	#ifdef DEBUG
		printf("INTERRUPT SWI MODE\n");
	#endif
    while(1);
}

void __attribute__ ((interrupt)) __cs3_isr_pabort (void)
{
	#ifdef DEBUG
		printf("INTERRUPT PABORT MODE\n");
	#endif
    while(1);
}

void __attribute__ ((interrupt)) __cs3_isr_dabort (void)
{
	#ifdef DEBUG
		printf("INTERRUPT DABORT MODE\n");
	#endif
    while(1);
}

void __attribute__ ((interrupt)) __cs3_isr_fiq (void)
{
	#ifdef DEBUG
		printf("INTERRUPT FIQ MODE\n");
	#endif
    while(1);
}

/* 
 * Initialize the banked stack pointer register for IRQ mode
*/
void set_A9_IRQ_stack(void)
{
	uint32_t stack, mode;
	stack = A9_ONCHIP_END - 7;		// top of A9 onchip memory, aligned to 8 bytes
	/* change processor to IRQ mode with interrupts disabled */
	mode = INT_DISABLE | IRQ_MODE;
	asm("msr cpsr, %[ps]" : : [ps] "r" (mode));
	/* set banked stack pointer */
	asm("mov sp, %[ps]" : : [ps] "r" (stack));

	/* go back to SVC mode before executing subroutine return! */
	mode = INT_DISABLE | SVC_MODE;
	asm("msr cpsr, %[ps]" : : [ps] "r" (mode));
}

/* 
 * Turn on interrupts in the ARM processor
*/
void enable_A9_interrupts(void)
{
	uint32_t status = SVC_MODE | INT_ENABLE;
	asm("msr cpsr, %[ps]" : : [ps]"r"(status));
}

/*
* Turn off interrupts in the ARM processor
*/
void disable_A9_interrupts(void) {
	uint32_t status = SVC_MODE | INT_DISABLE;
	asm("msr cpsr, %[ps]" : : [ps] "r"(status));
}

/*
* Configure Set Enable Registers (ICDISERn) and Interrupt Processor Target
* Registers (ICDIPTRn). The default (reset) values are used for other registers
* in the GIC.
*/
void config_interrupt(int N, int CPU_target) {
	int reg_offset, index, value, address;
	/* Configure the Interrupt Set-Enable Registers (ICDISERn).
	* reg_offset = (integer_div(N / 32) * 4
	* value = 1 << (N mod 32) */
	reg_offset = (N >> 3) & 0xFFFFFFFC;
	index = N & 0x1F;
	value = 0x1 << index;
	address = ICDISERn + reg_offset;
	/* Now that we know the register address and value, set the appropriate bit */
	*(volatile uint32_t *)address |= value;
	/* Configure the Interrupt Processor Targets Register (ICDIPTRn)
	* reg_offset = integer_div(N / 4) * 4
	* index = N mod 4 */
	reg_offset = (N & 0xFFFFFFFC);
	index = N & 0x3;
	address = ICDIPTRn + reg_offset + index;
	// Now that we know the register address and value, write to (only) the appropriate byte
	*(volatile char *)address = (char)CPU_target;
}

/* 
 * Configure the Generic Interrupt Controller (GIC)
*/
void config_GIC(void){
	/***********
	 * TO DO
	 **********/
	config_interrupt (TIMER_INT_NO, 1); // configure the timer interrupt (201)
	// Set Interrupt Priority Mask Register (ICCPMR). Enable interrupts of all priorities
	REG(ICCPMR) |= PMR_BITS;
	// Set CPU Interface Control Register (ICCICR). Enable signaling of interrupts
	REG(ICCICR) = ICR_BIT;
	// Configure the Distributor Control Register (ICDDCR) to send pending interrupts to CPUs
	REG(ICDDCR) |= DCR_BIT;
}

void Interrupt_init(){
	disable_A9_interrupts(); // disable interrupts in the A9 processor
    set_A9_IRQ_stack(); // initialize the stack pointer for IRQ mode
    config_GIC(); // configure the general interrupt controller
    enable_A9_interrupts(); // enable interrupts in the A9 processor
}
