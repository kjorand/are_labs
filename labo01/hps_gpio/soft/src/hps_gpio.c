/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : hps_gpio.c
 * Author               : 
 * Date                 : 
 *
 * Context              : ARE Introduction lab
 *
 *****************************************************************************************
 * Brief: light HPS user LED up when HPS user button pressed, for DE1-SoC board
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 
 *
*****************************************************************************************/
#include <stdint.h>
#include <stdio.h>
int __auto_semihosting;

#define GPIO_MOD0_BADDR 0xFF708000
#define GPIO_MOD1_BADDR 0xFF709000
#define GPIO_MOD2_BADDR 0xFF70A000

#define GPIO_MOD0_PIN_OFFSET 0
#define GPIO_MOD1_PIN_OFFSET -29
#define GPIO_MOD2_PIN_OFFSET -58

#define gpio_swporta_dr_offset 0x0 /* width : 32 RW 0x0 Port A Data Register*/
#define gpio_swporta_ddr_offset 0x4 /* width : 32 RW 0x0 Port A Data Direction Register*/
#define gpio_inten_offset 0x30 /* width : 32 RW 0x0 Interrupt Enable Register*/
#define gpio_intmask_offset 0x34 /* width : 32 RW 0x0 Interrupt Mask Register*/
#define gpio_inttype_level_offset 0x38 /* width : 32 RW 0x0 Interrupt Level Register*/
#define gpio_int_polarity_offset 0x3C /* width : 32 RW 0x0 Interrupt Polarity Register*/
#define gpio_intstatus_offset 0x40 /* width : 32 RO 0x0 Interrupt Status Register*/
#define gpio_raw_intstatus_offset 0x44 /* width : 32 RO 0x0 Raw Interrupt Status Register*/
#define gpio_debounce_offset 0x48 /* width : 32 RW 0x0 Debounce Enable Register*/
#define gpio_porta_eoi_offset 0x4C /* width : 32 WO 0x0 Clear Interrupt Register*/
#define gpio_ext_porta_offset 0x50 /* width : 32 RO 0x0 External Port A Register*/
#define gpio_ls_sync_offset 0x60 /* width : 32 RW 0x0 Synchronization Level Register*/
#define gpio_id_code_offset 0x64 /* width : 32 RO 0x0 ID Code Register*/
#define gpio_ver_id_code_offset 0x6C /* width : 32 RO 0x3230382A GPIO Version Register*/
#define gpio_config_reg2_offset 0x70 /* width : 32 RO 0x39CFC Configuration Register 2*/
#define gpio_config_reg1_offset 0x74 /* width : 32 RO 0x1FF0F2 Configuration Register 1*/

#define HPS_KEY_GPIO 54
#define HPS_LED_GPIO 53

#define GPIO_BIT(GPIO, OFFSET) ((unsigned)1 << (GPIO + OFFSET))

#define HPS_KEY_BIT (GPIO_BIT( HPS_KEY_GPIO, GPIO_MOD1_PIN_OFFSET ))
#define HPS_LED_BIT (GPIO_BIT( HPS_LED_GPIO, GPIO_MOD1_PIN_OFFSET ))


int main(void){
    
    printf("Test Labo introduction\n");

    // Paramètrage en entrée / sortie

    *((volatile uint32_t*)(GPIO_MOD1_BADDR + gpio_swporta_ddr_offset)) |= HPS_LED_BIT; // => set LED direction to 1
    *((volatile uint32_t*)(GPIO_MOD1_BADDR + gpio_swporta_ddr_offset)) &= ~(uint32_t)HPS_KEY_BIT; // => set KEY direction to 0 (default state)
    

    // Lecturebouton et report sur led
    
    uint32_t buttonPressed;
    while(1){
        buttonPressed = ! (*((volatile uint32_t*)(GPIO_MOD1_BADDR + gpio_ext_porta_offset)) & HPS_KEY_BIT);
        if(buttonPressed){
            *((volatile uint32_t*)(GPIO_MOD1_BADDR + gpio_swporta_dr_offset)) |= HPS_LED_BIT; 
        }else{
            *((volatile uint32_t*)(GPIO_MOD1_BADDR + gpio_swporta_dr_offset)) &= ~HPS_LED_BIT; 
        }
    }
}
