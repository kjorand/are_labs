------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : avl_user_interface.vhd
-- Author               :
-- Date                 : 04.08.2022
--
-- Context              : Avalon user interface
--
------------------------------------------------------------------------------------------
-- Description :
--
------------------------------------------------------------------------------------------
-- Dependencies :
--
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer    Comments
-- 0.0    See header              Initial version

------------------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity avl_user_interface is
  port(
    -- Avalon bus
    avl_clk_i           : in  std_logic;
    avl_reset_i         : in  std_logic;
    avl_address_i       : in  std_logic_vector(13 downto 0);
    avl_byteenable_i    : in  std_logic_vector(3 downto 0);
    avl_write_i         : in  std_logic;
    avl_writedata_i     : in  std_logic_vector(31 downto 0);
    avl_read_i          : in  std_logic;
    avl_readdatavalid_o : out std_logic;
    avl_readdata_o      : out std_logic_vector(31 downto 0);
    avl_waitrequest_o   : out std_logic;
    -- User interface
    button_i            : in  std_logic_vector(3 downto 0);
    switch_i            : in  std_logic_vector(9 downto 0);
    led_o               : out std_logic_vector(9 downto 0);
    -- Gen nombres
    nbr_a_i             : in  std_logic_vector(21 downto 0);
    nbr_b_i             : in  std_logic_vector(21 downto 0);
    nbr_c_i             : in  std_logic_vector(21 downto 0);
    nbr_d_i             : in  std_logic_vector(21 downto 0);
    cmd_init_o          : out std_logic;
    cmd_new_nbr_o       : out std_logic;
    auto_o              : out std_logic;
    delay_o             : out std_logic_vector(1 downto 0)
  );
end avl_user_interface;

architecture rtl of avl_user_interface is

    --| Components declaration |--------------------------------------------------------------

    component gestion_IO
    port(
        clk_i         : in  std_logic;
        reset_i       : in  std_logic;

        button_i      : in std_logic_vector(3 downto 0);
        switch_i      : in std_logic_vector(9 downto 0);
        led_i         : in std_logic_vector(9 downto 0);
        cs_led_wr_i   : in std_logic;

        button_sync_o : out std_logic_vector(3 downto 0);
        switch_sync_o : out std_logic_vector(9 downto 0);
        led_o         : out std_logic_vector(9 downto 0)
    );
    end component;

    component gestion_Gen
    port(
        clk_i        : in  std_logic;
        reset_i      : in  std_logic;

        cmd_new_i    : in  std_logic;
        cmd_init_i   : in  std_logic;
        cmd_save_i   : in  std_logic;
        cs_delay_i   : in  std_logic;
        delay_i      : in  std_logic_vector(1 downto 0);
        cs_auto_i    : in  std_logic;
        auto_i       : in  std_logic;
        cs_safe_i    : in  std_logic;
        safe_i       : in  std_logic;

        nbr_a_i      : in  std_logic_vector(21 downto 0);
        nbr_b_i      : in  std_logic_vector(21 downto 0);
        nbr_c_i      : in  std_logic_vector(21 downto 0);
        nbr_d_i      : in  std_logic_vector(21 downto 0);

        nbr_a_sync_o : out std_logic_vector(21 downto 0);
        nbr_b_sync_o : out std_logic_vector(21 downto 0);
        nbr_c_sync_o : out std_logic_vector(21 downto 0);
        nbr_d_sync_o : out std_logic_vector(21 downto 0);

        cmd_new_o    : out std_logic;
        cmd_init_o   : out std_logic;
        delay_o      : out std_logic_vector(1 downto 0);
        auto_o       : out std_logic;
        safe_o       : out std_logic

    );
    end component;


    --| Constants declarations |--------------------------------------------------------------
    constant HORS_ZONE : std_logic_vector(31 downto 0) := x"CAFEFECA";
    constant AVALON_ID : std_logic_vector(31 downto 0) := x"BEEFBEEF";
    constant RESERVED  : std_logic_vector(31 downto 0) := x"EEEEEEBA";
    constant UNUSED    : std_logic_vector(31 downto 0) := x"DEADCAFE";

    constant CODE_NBR_A_S : std_logic_vector(1 downto 0) := "00";
    constant CODE_NBR_B_S : std_logic_vector(1 downto 0) := "01";
    constant CODE_NBR_C_S : std_logic_vector(1 downto 0) := "10";
    constant CODE_NBR_D_S : std_logic_vector(1 downto 0) := "11";

    --| Signals declarations   |--------------------------------------------------------------
    signal button_s      : std_logic_vector(3 downto 0);
    signal switch_s      : std_logic_vector(9 downto 0);
    signal led_s         : std_logic_vector(9 downto 0);
    signal cs_led_wr_s   : std_logic;
    signal cs_ctr_gen_s  : std_logic;
    signal cs_ctr_safe_s : std_logic;
    signal cs_data_wr_s  : std_logic;

    signal cmd_new_s     : std_logic;
    signal cmd_init_s    : std_logic;

    signal mode_gen_s    : std_logic;
    signal safe_s        : std_logic;
    signal delay_gen_s   : std_logic_vector(1 downto 0);

    signal nbr_a_sync_s  : std_logic_vector(21 downto 0);
    signal nbr_b_sync_s  : std_logic_vector(21 downto 0);
    signal nbr_c_sync_s  : std_logic_vector(21 downto 0);
    signal nbr_d_sync_s  : std_logic_vector(21 downto 0);

begin

    -- ####################
    -- Read access part
    -- ####################

    read_channel : process (avl_clk_i, avl_reset_i) is
    begin

        if avl_reset_i = '1' then
            avl_readdatavalid_o <= '0';
            avl_readdata_o <= (others => '0');


        elsif rising_edge(avl_clk_i) then

            if avl_read_i = '1' then

                if avl_address_i(13 downto 8) = "000000" then -- in Zone

                    avl_readdata_o <= (others => '0');

                    case avl_address_i(7 downto 0) is
                        when x"00" => -- CPU addr : 00
                            avl_readdata_o               <= AVALON_ID;
                        when x"01" => -- CPU addr : 04
                            avl_readdata_o(3 downto 0)   <= button_s;
                        when x"02" => -- CPU addr : 08
                            avl_readdata_o(9 downto 0)   <= switch_s;
                        when x"03" => -- CPU addr : 0C
                            avl_readdata_o(9 downto 0)   <= led_s;
                        when x"04" => -- CPU addr : 10
                            avl_readdata_o(1)            <= '1'; -- status_s; -- [1] : indique si l'interf. a une capture des 4 nombres (photo instantanée)
                            avl_readdata_o(0)            <= safe_s; -- status_s; -- [0] : indique qu'une photo a été prise.
                        when x"05" => -- CPU addr : 14
                            avl_readdata_o(4)            <= mode_gen_s;
                            avl_readdata_o(1 downto 0)   <= delay_gen_s;
                        when x"06" => -- CPU addr : 18
                            avl_readdata_o(0)            <= safe_s;
                        when x"07" => -- CPU addr : 1C
                            avl_readdata_o               <= RESERVED;
                        when x"08" => -- CPU addr : 20
                            avl_readdata_o(21 downto 0)  <= nbr_a_sync_s;
                            avl_readdata_o(23 downto 22) <= CODE_NBR_A_S;
                        when x"09" => -- CPU addr : 24
                            avl_readdata_o(21 downto 0)  <= nbr_b_sync_s;
                            avl_readdata_o(23 downto 22) <= CODE_NBR_B_S;
                        when x"0A" => -- CPU addr : 28
                            avl_readdata_o(21 downto 0)  <= nbr_c_sync_s;
                            avl_readdata_o(23 downto 22) <= CODE_NBR_C_S;
                        when x"0B" => -- CPU addr : 2C
                            avl_readdata_o(21 downto 0)  <= nbr_d_sync_s;
                            avl_readdata_o(23 downto 22) <= CODE_NBR_D_S;
                        when others =>
                            avl_readdata_o               <= UNUSED;
                    end case;

                else -- Out of Zone
                    avl_readdata_o   <= HORS_ZONE;

                end if;

                avl_readdatavalid_o <= '1';

            else
                avl_readdatavalid_o <= '0';

            end if;

        end if;

    end process;

    -- ####################
    -- Write access part
    -- ####################

    write_channel : process (avl_clk_i, avl_reset_i)
    begin

        if avl_reset_i = '1' then

            cs_led_wr_s  <= '0';
            cs_ctr_gen_s <= '0';
            cs_data_wr_s <= '0';
            cs_ctr_safe_s <= '0';

        elsif rising_edge(avl_clk_i) then

            cs_led_wr_s  <= '0';
            cs_ctr_gen_s <= '0';
            cs_data_wr_s <= '0';
            cs_ctr_safe_s <= '0';


            if avl_write_i = '1' then
                if avl_address_i(13 downto 8) = "000000" then
                    case avl_address_i(7 downto 0) is
                        when x"03" => -- CPU addr : 0C
                            cs_led_wr_s   <= '1';

                        when x"04" => -- CPU addr : 10
                            cs_ctr_gen_s  <= '1';

                        when x"05" => -- CPU addr : 14
                            cs_data_wr_s  <= '1';

                        when x"06" => -- CPU addr : 18
                            cs_ctr_safe_s <= '1';

                        when x"07" => -- CPU addr : 1C
                            -- a completer

                        when others =>
                            -- for simulation
                    end case;
                end if;
            end if;
        end if;

    end process;

    -- ####################
    -- Interface management
    -- ####################

    gestion_Gen_avl_user_interface : gestion_Gen
        port map (
            clk_i        => avl_clk_i,
            reset_i      => avl_reset_i,

            cmd_new_i    => cs_ctr_gen_s and avl_writedata_i(4),
            cmd_init_i   => cs_ctr_gen_s and avl_writedata_i(0),
            cmd_save_i   => cs_ctr_safe_s and avl_writedata_i(1),
            cs_delay_i   => cs_data_wr_s,
            delay_i      => avl_writedata_i(1 downto 0),
            cs_auto_i    => cs_data_wr_s,
            auto_i       => avl_writedata_i(4),
            cs_safe_i    => cs_ctr_safe_s,
            safe_i       => avl_writedata_i(0),

            nbr_a_i      => nbr_a_i,
            nbr_b_i      => nbr_b_i,
            nbr_c_i      => nbr_c_i,
            nbr_d_i      => nbr_d_i,

            nbr_a_sync_o => nbr_a_sync_s,
            nbr_b_sync_o => nbr_b_sync_s,
            nbr_c_sync_o => nbr_c_sync_s,
            nbr_d_sync_o => nbr_d_sync_s,

            cmd_new_o    => cmd_new_s,
            cmd_init_o   => cmd_init_s,
            delay_o      => delay_gen_s,
            auto_o       => mode_gen_s,
            safe_o       => safe_s
        );

    -- ####################
    -- Gestion IO DE1-SoC
    -- ####################

    gestion_IO_avl_user_interface : gestion_IO
        port map (
            clk_i         => avl_clk_i,
            reset_i       => avl_reset_i,

            button_i      => button_i,
            switch_i      => switch_i,
            cs_led_wr_i   => cs_led_wr_s,
            led_i         => avl_writedata_i(9 downto 0),

            button_sync_o => button_s,
            switch_sync_o => switch_s,
            led_o         => led_s
        );


    -- ####################
    -- Output drive
    -- ####################

    -- IO
    led_o <= led_s;

    -- Gen
    cmd_init_o <= cmd_init_s;
    cmd_new_nbr_o <= cmd_new_s;
    auto_o <= mode_gen_s;
    delay_o <= delay_gen_s;

    -- AVL
    avl_waitrequest_o <= '0'; -- unused

end rtl;
