------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
--
-- File                 : gestion_Gen.vhd
-- Author               :
-- Date                 : 07.11.2023
--
-- Context              : Avalon user interface
--
------------------------------------------------------------------------------------------
-- Description :
--
------------------------------------------------------------------------------------------
-- Dependencies :
--
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer    Comments
-- 0.0    See header              Initial version

------------------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity gestion_Gen is
    port(
        clk_i        : in  std_logic;
        reset_i      : in  std_logic;

        cmd_new_i    : in  std_logic;
        cmd_init_i   : in  std_logic;
        cmd_save_i   : in  std_logic;
        cs_delay_i   : in  std_logic;
        delay_i      : in  std_logic_vector(1 downto 0);
        cs_auto_i    : in  std_logic;
        auto_i       : in  std_logic;
        cs_safe_i    : in  std_logic;
        safe_i       : in  std_logic;

        nbr_a_i      : in  std_logic_vector(21 downto 0);
        nbr_b_i      : in  std_logic_vector(21 downto 0);
        nbr_c_i      : in  std_logic_vector(21 downto 0);
        nbr_d_i      : in  std_logic_vector(21 downto 0);

        nbr_a_sync_o : out std_logic_vector(21 downto 0);
        nbr_b_sync_o : out std_logic_vector(21 downto 0);
        nbr_c_sync_o : out std_logic_vector(21 downto 0);
        nbr_d_sync_o : out std_logic_vector(21 downto 0);

        cmd_new_o    : out std_logic;
        cmd_init_o   : out std_logic;
        delay_o      : out std_logic_vector(1 downto 0);
        auto_o       : out std_logic;
        safe_o       : out std_logic

    );
end gestion_Gen;

architecture rtl of gestion_Gen is
    --| Signals declarations   |--------------------------------------------------------------
    signal safe_s : std_logic;

begin
    process (clk_i, reset_i)
    begin
        if reset_i = '1' then
            nbr_a_sync_o <= (others => '0');
            nbr_b_sync_o <= (others => '0');
            nbr_c_sync_o <= (others => '0');
            nbr_d_sync_o <= (others => '0');

            cmd_new_o    <= '0';
            cmd_init_o   <= '0';
            delay_o      <= (others => '0');
            auto_o       <= '0';
            safe_s       <= '0';

        elsif rising_edge(clk_i) then
            -- Flip-flops
            cmd_new_o    <= cmd_new_i;
            cmd_init_o   <= cmd_init_i;

            -- Registers
            if cs_delay_i = '1' then
                delay_o <= delay_i;
            end if;

            if cs_auto_i = '1' then
                auto_o <= auto_i;
            end if;

            if cs_safe_i = '1' then
                safe_s <= safe_i;
            end if;

            if safe_s = '0' or cmd_save_i = '1' then
                nbr_a_sync_o <= nbr_a_i;
                nbr_b_sync_o <= nbr_b_i;
                nbr_c_sync_o <= nbr_c_i;
                nbr_d_sync_o <= nbr_d_i;
            end if;

        end if;
    end process;

    safe_o <= safe_s;

end rtl;