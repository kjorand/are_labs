/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : io_functions.h
 * Author               : Leupi & Jorand
 * Date                 : 06.11.2023
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: Header file for io functions
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 0.0    06.11.2023  RLI & KJD    Initial version.
 *
*****************************************************************************************/

#ifndef __IO_FUNCTIONS_H__
#define __IO_FUNCTIONS_H__

#include <stdint.h>

//***********************************//
//****** Global usage function ******//

/// @brief Reads the keys register and fills the state and edge passed in parameter as long as not null.
/// Information is represented as bitfield : each bit representing one key
/// @param state receives the current state of the buttons
/// @param rising_edge  receives the rising edge of the buttons : every button which saw a rising edge since last call is set to '1' others to '0'
void read_key(uint8_t *state, uint8_t *rising_edge);

/// @brief Reads the value on the switches
/// @return The state of the switches, one bit for each
uint16_t read_switch();

/// @brief Puts the argument on the leds (each led representing one bit for the LSBs)
/// @param leds The value to put on leds
void set_leds(uint16_t leds);

/// @brief Reads the avalon ID constant
/// @return The avalon ID constant
uint32_t read_avalon_ID();

/// @brief Reads the AXI lightweight bus constant ID
/// @return The AXI lightweight bus constant ID
uint32_t read_axi_lw_ID();

/// @brief Reads the status according to :
/// status : Deux bits de statut, placés aux bits [1..0] (Ils seront utilisés lors de la seconde partie du laboratoire) :
///     o status[1] : indique si l'interface dispose d'un système de capture des 4 nombres (photo instantanée). Si status[1] = '1' système capture disponible.
///     o status[0] : indique qu'une photo a été prise lorsque le système de capture est actif. Ce bit est valide uniquement si status[1] = '1'.
/// @return the status on the 2 LSBs
uint8_t read_status();

/// @brief Sends the init command to the generator
void init_gen();

/// @brief Sends the new number command to the generator
void new_nbr();

/// @brief Reads the mode of the generator
/// @return the mode of the generator on the 1 LSB : ‘0’ : mode manuel / ‘1’ : mode automatique
uint8_t read_mode_gen();

/// @brief Reads the "delay" of the generator (actually more like it's frequency)
/// @return the frequency code on the 2 LSBs :
///    - "00" : 1 Hz
///    - "01" : 1 KHz
///    - "10" : 100 KHz
///    - "11" : 1 MHz
uint8_t read_delay_gen();

/// @brief Selects generator mode from : ‘0’ : mode manuel / ‘1’ : mode automatique
/// @param val the mode of the generator on the 1 LSB
void write_mode_gen(uint8_t val);

/// @brief Selects the "delay" of the generator (actually more like it's frequency) according to :
///    - "00" : 1 Hz
///    - "01" : 1 KHz
///    - "10" : 100 KHz
///    - "11" : 1 MHz
/// @param val the frequency code on the 2 LSBs
void write_delay_gen(uint8_t val);

/// @brief Selects the mode and delay as with write_mode_gen and write_delay_gen but in one writing operation
/// @param mode the mode of the generator on the 1 LSB
/// @param delay the frequency code on the 2 LSBs
void write_mode_delay_gen(uint8_t mode, uint8_t delay);

/// @brief Reads the nbr from the generator : generic function letting the user read any of the 4 numbers
/// @param no the no of the number (0-3 or 'a'-'d')
/// @return The actual number, without it's code
uint32_t read_nbr(uint8_t no);

/// @brief Writes the 2 LSBs to the safe_mode control register
/// @param val the value written to the register :
///     [0] is the safe mode
///     [1] is the save command
void write_safe_mode(uint8_t val);

#endif /* __IO_FUNCTIONS_H__ */
