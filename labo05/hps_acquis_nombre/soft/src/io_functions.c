/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : io_functions.c
 * Author               : Leupi & Jorand
 * Date                 : 06.11.2023
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: io functions
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 0.0    06.11.2023  RLI & KJD    Initial version.
 *
*****************************************************************************************/

#include <stdint.h>
#include "axi_lw_avalon.h"
#include "io_functions.h"
#include <strings.h>

#define GET_SHIFT_FROM_REG(_REG_, _BITS_) ((AVALON_REG(_REG_) & (_BITS_)) >> (ffs(_BITS_)-1))


void read_key(uint8_t *state, uint8_t *rising_edge) {
    static uint8_t lastValue = 0x0;
    uint8_t currentValue = (AVALON_REG(REG_BOUTONS) & KEY_BITS);
    if(state){
        *state = currentValue;
    }
    if(rising_edge){
        *rising_edge = (~lastValue) & currentValue; // forces last = 0 and current = 1
    }

    lastValue = currentValue;
}

uint16_t read_switch(){
    return (uint16_t)(AVALON_REG(REG_SWITCHS) & SWITCHS_BITS);
}

void set_leds(uint16_t leds){
    AVALON_REG(REG_LEDS) = (AVALON_REG(REG_LEDS) & ~LEDS_BITS) | (leds & LEDS_BITS);
}

uint32_t read_avalon_ID(){
    return AVALON_REG(REG_AVALONID);
}

uint32_t read_axi_lw_ID(){
    return AXI_LW_REG(REG_AXI_LW_ID);
}

uint8_t read_status(){
    return GET_SHIFT_FROM_REG(REG_COMMAND, STATUS_BITS);
}

void init_gen(){
    AVALON_REG(REG_COMMAND) = INIT_NBR_BITS; // Plan d'adressage = not good : if we "or" the bits in that register ... then if status[0] is set (number saved) it WILL re-init the generator ... (even though we have many available addresses AND also stilll MOST bits available in the register)
}

void new_nbr(){
    AVALON_REG(REG_COMMAND) = NEW_NBR_BITS; // Plan d'adressage = not good : if we "or" the bits in that register ... then if status[0] is set (number saved) it WILL re-init the generator ... (even though we have many available addresses AND also stilll MOST bits available in the register)
}

uint8_t read_mode_gen(){
    return GET_SHIFT_FROM_REG(REG_MODE_DELAY, MODE_GEN_BITS);
}

uint8_t read_delay_gen(){
    return GET_SHIFT_FROM_REG(REG_MODE_DELAY, DELAY_GEN_BITS);
}

void write_mode_gen(uint8_t val){
    AVALON_REG(REG_MODE_DELAY) = (AVALON_REG(REG_MODE_DELAY) & ~MODE_GEN_BITS) | ( (val << ffs(MODE_GEN_BITS) ) & MODE_GEN_BITS);
}

void write_delay_gen(uint8_t val){
    AVALON_REG(REG_MODE_DELAY) = (AVALON_REG(REG_MODE_DELAY) & ~DELAY_GEN_BITS) | (val << ffs(DELAY_GEN_BITS) & DELAY_GEN_BITS);
}

void write_mode_delay_gen(uint8_t mode, uint8_t delay){
    AVALON_REG(REG_MODE_DELAY) = (AVALON_REG(REG_MODE_DELAY) & ~(DELAY_GEN_BITS | MODE_GEN_BITS) ) | ( (delay << (ffs(DELAY_GEN_BITS)-1) ) & DELAY_GEN_BITS) | ( (mode << (ffs(MODE_GEN_BITS)-1) ) & MODE_GEN_BITS);
}

uint32_t read_nbr(uint8_t no){
    switch (no){
        case 'a':
        case 0:
            return GET_SHIFT_FROM_REG(REG_NBR_A, NBR_BITS);

        case 'b':
        case 1:
            return GET_SHIFT_FROM_REG(REG_NBR_B, NBR_BITS);

        case 'c':
        case 2:
            return GET_SHIFT_FROM_REG(REG_NBR_C, NBR_BITS);

        case 'd':
        case 3:
            return GET_SHIFT_FROM_REG(REG_NBR_D, NBR_BITS);

        default:
            return 0;
    }
}

void write_safe_mode(uint8_t val){
	AVALON_REG(REG_CTRL_SAFE) = (AVALON_REG(REG_CTRL_SAFE) & ~(SAVE_BITS | SAFE_MODE_BITS) ) | (val & (SAVE_BITS | SAFE_MODE_BITS));
}
