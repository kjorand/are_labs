/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : hps_application.c
 * Author               :
 * Date                 :
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: Conception d'une interface évoluée sur le bus Avalon avec la carte DE1-SoC
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 *
 *
*****************************************************************************************/
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include "axi_lw_avalon.h"
#include "io_functions.h"

int __auto_semihosting;

/// @brief Sum numbers and only takes the number of bits they're made out of
/// @param a the first generated number
/// @param b the second generated number
/// @param c the third generated number
/// @return The sum of the 3 numbers ... masked with their size
/// @note As an alternative, we could have also passed "d" and returned a bool indicating if the sum corresponds (first implementation) ... but then we also need the sum value ... so we'd have needed a fifth ("return") parameter ... which might have made it more complicated than necessary ...
uint32_t sum_nbr(uint32_t a, uint32_t b, uint32_t c){
    return (a + b + c) & NBR_BITS;
}

int main(void){

    printf("Laboratoire: Conception d'une interface évoluée \n");

    // Turn all DE1-SoC leds off
    set_leds(0x0000);
    init_gen(); // init the number generator

    // Read and print constants / ID
    printf("AXI ID : 0x%"PRIX32" \n", read_axi_lw_ID());
    printf("AVALON ID : 0x%"PRIX32" \n", read_avalon_ID());

    // END INIT !

    uint8_t keys;
    uint8_t keys_pressed;
    uint16_t switchs;
    uint32_t nbr[4]; // Numbers that have been read
    uint32_t err_cnt = 0;
    uint32_t sum;
    while(1){
        // Read keys
        read_key(&keys, &keys_pressed); // Love it when you can just change 1 var and your prog behaviour changes and complies with assistant expectations !!

        // Read switchs
        switchs = read_switch();

        // Report switchs on leds
        set_leds(switchs);

        if(keys_pressed & KEY0){ // Key 0 pressed
            init_gen();
        }

        if(keys_pressed & KEY1 && read_mode_gen() == MANUAL){ // Key 1 pressed and in manual mode
            new_nbr();
        }

        if(keys & KEY2){ // Key 2 is down
            for(size_t i = 0; i < 4; ++i){ // Read the 4 numbers
                nbr[i] = read_nbr(i);
            }
            sum = sum_nbr(nbr[0], nbr[1], nbr[2]); // Sum them and take into account their max bit value (on 20 bits, not 32 ... )
            if(sum == nbr[3]){ // Check validity
                printf("OK : status: 0x%"PRIX8" , somme: %"PRIu32", nbr_a: %"PRIu32", nbr_b: %"PRIu32", nbr_c: %"PRIu32", nbr_d: %"PRIu32"\n", read_status(), sum, nbr[0], nbr[1], nbr[2], nbr[3]);
            }else{
                ++ err_cnt;
                printf("ER : status: 0x%"PRIX8" , somme: %"PRIu32", nbr_a: %"PRIu32", nbr_b: %"PRIu32", nbr_c: %"PRIu32", nbr_d: %"PRIu32"\n", read_status(), sum, nbr[0], nbr[1], nbr[2], nbr[3]);
                printf("ER : nombre d'erreur cumulée : %"PRIu32"\n", err_cnt);
            }
        }

        write_mode_delay_gen( (switchs >> 7) & 0x1, (switchs >> 8) & 0x3 ); // Write the delay (could be conditionned to the auto mode, but we have time ... )

        write_safe_mode( (switchs & 0x1) << 1 | (switchs & 0x1) ); // Write safe mode and asks for a save in case of safe mode is activated ... (unused otherwise)
    }
}
