/*****************************************************************************************
 * HEIG-VD
 * Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
 * School of Business and Engineering in Canton de Vaud
 *****************************************************************************************
 * REDS Institute
 * Reconfigurable Embedded Digital Systems
 *****************************************************************************************
 *
 * File                 : axi_lw.h
 * Author               : Anthony Convers
 * Date                 : 27.07.2022
 *
 * Context              : ARE lab
 *
 *****************************************************************************************
 * Brief: Header file for bus AXI lightweight HPS to FPGA defines definition
 *
 *****************************************************************************************
 * Modifications :
 * Ver    Date        Student      Comments
 * 0.0    27.07.2022  ACS           Initial version.
 *
*****************************************************************************************/

#ifndef __AXI_LW_AVALON_H__
#define __AXI_LW_AVALON_H__

#include <stdint.h>

// Base address
#define AXI_LW_HPS_FPGA_BASE_ADD    0xFF200000 /* source : p. 2-16 */
#define AVALON_BASE_ADD             0x00010000

// AXI registers
#define REG_AXI_LW_ID  0x0000

// Avalon registers
#define REG_AVALONID   0x0000 /* AvalonID[31..0]                                                / Reserved                                                           -- ID du bus Avalon                */
#define REG_BOUTONS    0x0004 /* Boutons state[3..0] - Unused[31..4]                            / Reserved                                                           -- DE1-SoC - Boutons               */
#define REG_SWITCHS    0x0008 /* Switchs state[9..0] - Unused[31..10]                           / Reserved                                                           -- DE1-SoC - Switchs               */
#define REG_LEDS       0x000C /* Leds[9..0] - Unused[31..10]                                    / Leds[9..0] - Reserved[31..10]                                      -- DE1-SoC - Leds                  */
#define REG_COMMAND    0x0010 /* Status[1..0] - Unused[31..2]                                   / [31..5] reserved ; [4] new_nbr ; [3..1] reserved ; [0] init_nbr    -- Gen - Status                    */
#define REG_MODE_DELAY 0x0014 /* [31..5] Unused ; [4] mode_gen ; [3-2] unused ; [1-0] delay_gen / [31..5] reserved ; [4] mode_gen ; [3-2] reserved ; [1-0] delay_gen -- Gen - Transmission Status       */
#define REG_CTRL_SAFE  0x0018 /* [31..1] Unused ; [0] safe_mode (same as status(0)              / [31..2] reserved ; [1] save command ; [0] safe_mode                -- write 1 to [1] takes snapshot   */
#define REG_AVAIL2     0x001C /* available for new functionality                                / available for new functionality                                    -- available for new functionality */
#define REG_NBR_A      0x0020 /* [31..24] unused [23-22] "00" (code nbr_a) [21..0] valeur nbr_a / reserved                                                           -- Gen - nbr_a                     */
#define REG_NBR_B      0x0024 /* [31..24] unused [23-22] "01" (code nbr_b) [21..0] valeur nbr_b / reserved                                                           -- Gen - nbr_b                     */
#define REG_NBR_C      0x0028 /* [31..24] unused [23-22] "10" (code nbr_c) [21..0] valeur nbr_c / reserved                                                           -- Gen - nbr_c                     */
#define REG_NBR_D      0x002C /* [31..24] unused [23-22] "11" (code nbr_d) [21..0] valeur nbr_d / reserved                                                           -- Gen - nbr_d                     */

// ACCESS MACROS
#define AVALON_REG(_x_)   *(volatile uint32_t *)(AXI_LW_HPS_FPGA_BASE_ADD + AVALON_BASE_ADD + _x_) // _x_ is a "CPU" offset with respect to the avalon base address
#define AXI_LW_REG(_x_)   *(volatile uint32_t *)(AXI_LW_HPS_FPGA_BASE_ADD + _x_) /* _x_ is a "CPU" offset with respect to the base address */

// Define bits usage
#define SWITCHS_BITS   0x000003FF
#define LEDS_BITS      0x000003FF
#define KEY_BITS       0x0000000F

#define STATUS_BITS    0x00000003
#define NEW_NBR_BITS   0x00000010
#define INIT_NBR_BITS  0x00000001
#define MODE_GEN_BITS  0x00000010
#define DELAY_GEN_BITS 0x00000003
#define NBR_CODE_BITS  0x00C00000
#define NBR_BITS       0x003FFFFF
#define SAVE_BITS      0x00000002
#define SAFE_MODE_BITS 0x00000001

// Individual key's bits
#define KEY0 0x1
#define KEY1 0x2
#define KEY2 0x4
#define KEY3 0x8

// Define values for the parameters ...
#define FREQ_1H   0 /* "00" : 1 Hz    */
#define FREQ_1K   1 /* "01" : 1 KHz   */
#define FREQ_100K 2 /* "10" : 100 KHz */
#define FREQ_1M   3 /* "11" : 1 MHz   */

#define MANUAL 0
#define AUTO   1

#endif /* __AXI_LW_AVALON_H__ */
