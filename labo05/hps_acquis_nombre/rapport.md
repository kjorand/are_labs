
# Laboratoire 5 - Conception d'une interface fiable
Auteurs : Kévin Jorand, Rachel Leupi

## Objectifs :
Le but de ce laboratoire est de créer une interface robuste sur le bus Avalon connecté au HPS. Pour y parvenir, il est nécessaire de concevoir une interface entre un générateur de nombres et la carte de développement DE1-SoC via le bus Avalon.

Les objectifs principaux sont les suivants :

- Identifier les erreurs d'acquisition en l'absence de mesures de sécurité.
- Mettre en place une acquisition fiable des 4 nombres afin d'assurer la cohérence optimale du système.

## Architecture VHDL commune :

Voici une description des différents éléments constituant notre interface qui est commune aux deux solutions (avec et sans acquisition fiable).

### Synchronisation des entrée :

![image](Images/gestion_io.JPG)
Ce module permet la synchronisation des entrées provenant de la DE1-Soc tel que les boutons, les switchs, ainsi que de contrôler l'écriture sur les leds.

### Write Access Part
Ce bloc d'éléments logiques est en charge de récupérer les informations transmises via le bus Avalon et d'appliquer la valeur sur le registre demandé en écriture.

Pour cela voici un schéma :
![image](Images/WAP.JPG)

Le décodeur d'adresses répond au équations suivantes :
```vhd
cs_led_wr_s = (avl_address_i == 0x0003) & avl_write_i & !avl_reset_i
cs_ctr_gen_s = (avl_address_i == 0x0004) & avl_write_i & !avl_reset_i
cs_data_wr_s = (avl_address_i == 0x0005) & avl_write_i & !avl_reset_i
cs_ctr_safe_s = (avl_address_i == 0x0006) & avl_write_i & !avl_reset_i
```
Les bits du signal write_data_i seront directement appliqués aux entrées concernées selon les "chip select" concernés.

### Read Access Part

Ce bloc d'éléments logiques est en charge de récupérer les informations du système (DE1-So, générateur de nombres), et de les transmettre via le bus Avalon au registre pour les mettre à jour.

![image](Images/RAP.JPG)

Le décodeur d'adresses répond aux équations suivantes :
```
avl_readdatavalid_o = (avl_read_i == 1)

avl_readdata_o =
  si avl_address_i > 0x00FF:
    HORS_ZONE
  sinon si avl_address_i == 0x0000 :
    AVALON_ID;
  sinon si avl_address_i == 0x0001 :
    button_s;
  sinon si avl_address_i == 0x0002 :
    switch_s;
  sinon si avl_address_i == 0x0003 :
    led_s;
  sinon si avl_address_i == 0x0004 :
    0x00000001 && safe_s;
  sinon si avl_address_i == 0x0005 :
    0x00000000 && (mode_gen_s << 4) && (delay_gen_s);
  sinon si avl_address_i == 0x0006 :
    safe_s;
  sinon si avl_address_i == 0x0007 :
    RESERVED;
  sinon si avl_address_i == 0x0008 :
    (CODE_NBR_A_S << 22) && nbr_a_sync_s;
  sinon si avl_address_i == 0x0009 :
    (CODE_NBR_B_S << 22) && nbr_b_sync_s;
  sinon si avl_address_i == 0x000A :
    (CODE_NBR_C_S << 22) && nbr_c_sync_s;
  sinon si avl_address_i == 0x000B :
    (CODE_NBR_D_S << 22) && nbr_d_sync_s;
  sinon :
    UNUSED;
```

## 1ère Partie :
###  VHDL :

#### 1) Design pour une lecture non-fiable des nombres :

![image](Images/gestionGen.JPG)

Ce bloc permet la synchronisation des nombres générés, ainsi que des signaux `cmd_init` et `cmd_new`.

Les signaux `delay` et `auto` permettent de piloter le générateur de nombre, le signal `delay` représente la fréquence de génération et le signal `auto` pilote le mode de fonctionnement, si `auto` vaut 1 alors le générateur génère un nombre selon la fréquence, sinon génère un nombre si demandé.

#### 2) Test :

![Images]( Images/Capture4.JPG)
Ici on peut constater que :
- La constante de notre interface est lue correctement à l'adresse 0x0.
- La lecture des switchs  se fait correctement à l'adresse 0x8.
- La lecture des boutons se fait correctement à l'adresse 0x4. Leur polarité a été inversée pour plus de clarté (physiquement : actif-bas, valeurs lues : actif-haut).
- La lecture des leds se fait correctement à l'adresse 0xC.

Elles sont effectivement éteintes.

![Images]( Images/Capture6.JPG)
Ici on constate que :
- L'écriture sur les leds se fait correctement. Elles sont dorénavant allumées.

Les lecture de status / génération de nombres ont aussi étés testées et fonctionnent comme attendu.

### Programme C :
#### 1) Fonctionnement :
Nous avons essentiellement repris le code de base d'anciens labos. Nous commençons par les initialisations (y compris du générateur de nombres) et les prints initiaux dans la console.

Nous avons ensuite une fonction qui va lire l'état des boutons et s'occupe en même temps de la détection de flancs de ces derniers ! On lit et stock l'état des switchs, que nous reportons directement sur les leds.

Si les boutons 0 / 1 ont été pressés, on effectue les actions associées. En l’occurrence on appelle simplement directement 2 fonctions IO qui permettent de donner les commandes correspondantes (`init` et `new_nbr`) au générateur. Si le bouton 2 a été pressé, alors on lit les 4 nombres consécutivement et on calcule la somme des 3 premiers. On compare si cette dernière correspond au 4ème et on affiche les messages correspondants dans la console.

Finalement, on "set" la fréquence du générateur selon l'état des switchs.

#### 2) Test :
Nous avions un code suffisamment simple pour pouvoir tester directement nos fonctions IO. Nous avons pu vérifier le bon fonctionnement des lectures / écritures des ports IO de la DE1-SoC immédiatement (code directement repris des labos précédents). Nous avons ensuite pu tester les lectures, les écritures et enfin les commandes au générateur. Nous avons à chaque fois pu observer les résultats escomptés, que ce soit via l'observation via la partie "memory" ou via le programme qui tourne (plus simple par exemple pour observer le bon fonctionnement du réglage de la fréquence de génération des nombres).

## 2ème Partie :
###  VHDL :
#### 1) Design pour une lecture fiable des nombres :
Nous avons très rapidement remarqué que le concept était suffisamment simpliste pour n'être réalisé qu'à l'aide d'une ou 2 portes logiques, sans nécessiter de MSS.

Nous avons ainsi simplement ajouté un registre enregistrant le mode safe où non ... et une commande pour demander une nouvelle sauvegarde. Ce qui était auparavant des flip-flops pour synchroniser les nombres deviennent alors des registres. Ces derniers sont sensibles aux inputs uniquement si le mode "unsafe" est activé ou une sauvegarde a été demandée. La logique est ainsi extrêmement simple.

![image](Images/gestionGenv2.png)

#### 2) Test :
Nous avons à nouveau testé le tout dans le simulateur (en particulier les commandes et les lectures/écritures liées au gestionnaire du générateur de nombres ... ). Nous avons vérifié avoir une lecture fiable. Évidemment, nous n'avons pas pu tester aussi "in depth" dans le simulateur qu'en C. C'est pourquoi nous avons encore trouvé des erreurs en testant avec le programme C (cf  ci-dessous).

### Programme C :
#### 1) Fonctionnement :
Le fonctionnement est basiquement le même avec l'ajout du report de la valeur lue sur le switch 0 comme valeur d'activation/désactivation du "safe mode". On envoie aussi systématiquement une commande de lecture / sauvegarde. En effet, soit on est en mode safe et ... on aimerait bien lire les prochains nombres ... soit on ne l'est pas et cela n'aura aucun effet. Il n'y a donc aucun bénéfice en l'état actuel à faire un traitement de cas particulier...

#### 2) Test :
Nous avions déjà l'appli C prête, fonctionnelle et testée et en avons donc profité pour tester directement depuis l'appli (application du safemode pendant la lecture, initialisation, commande de nouveau nombre, ... ). C'est ainsi que nous avons trouvé la dernière erreur (cf ci-dessous, la seconde).

#### 3) Erreurs :
Pendant le développement / test de notre labo, nous nous sommes heurtés à deux problèmes principaux. Nous détaillons ci-dessous ce qu'il s'est passé, le problème/l'erreur et comment le résoudre ainsi que la raison qui l'a rendu difficile à cerner pour nous (alors que tous les deux sont relativement triviaux !).

**Non-réinitialisation à 0 des flags dans le VHDL** (part 2; ajout) : Une erreur d'inattention lors de la copie des paramètres nous a mis dans une situation où nos "flags" internes n'étaient pas mis à 0 alors qu'ils le devaient (type flip-flop ...). Nous avions des lectures contradictoires dans le programme VS dans l'onglet MEMORY. En effet, ce dernier ne fait "que" lire chacun des emplacements mémoire; ainsi à chaque breakpoint, il faisait une nouvelle lecture, qui venait ainsi fausser ce que nous attentions. Impossible non-plus (avant d'avoir saisi le problème de la lecture dans l'onglet memory) de pouvoir reproduire le problème dans le simulateur. Nous tentions alors de décortiquer en vain (puisqu'il était fonctionnel) ce qui se passait dans le programme C ...

**"or'ing" registers** (part 2; tests) : Une erreur de manipulation C nous a aussi fait suer quelques gouttes lors des tests. En effet, en mode manuel, en passant en mode safe, la demande d'un nouveau nombre aboutissait systématiquement à une nouvelle initialisation du générateur de nombres. Nous avons finis par trouver que nous faisions un "ou-égal" sur le registre (pour ne mettre à 1 que les bits souhaités et ne pas impacter les autres), ce qui est une pratique plutôt courante.

Sachant qu'il s'agissait de commandes, seul nous intéressait de mettre ce bits à 1 (on savait qu'on ne mémorisait pas l'état et qu'on ne devait ainsi pas recevoir son état) et non l'inverse d'où le "ou-égal". Nous nous sommes finalement rendus compte que la commande d'initialisation concordait dans le plan d'adressage avec le bit de statut 0. Ainsi lorsque ce dernier était "set", au moment de faire un "ou-égal", cela envoyait une commande d'initialisation. Nous avions identifié le problème potentiel dans la partie 1, mais étant asymptomatique puisque ce bit de statut était toujours à 0 dans la partie 1, il a vite été oublié.

Outre le fait de nous avoir bien servi de leçon, en application réelle, un tel plan d'adressage est une _très_ mauvaise idée ... précisément en raison du potentiel de problèmes générés. Ce d'autant plus que de nombreuses autres adresses demeurent disponibles ... et même de nombreux autres bits restent inutilisés dans ce même registre.

## Conclusion
En conclusion, nous avons pu observer et même expérimenter certaines erreurs pas toujours faciles à trouver / diagnostiquer, ce qui nous fait profiter d'un gain d'expérience non-négligeable. Nous avons aussi apprécié sa relative faible complexité intrinsèque tout en abordant un thème (lecture fiable) relativement important.
